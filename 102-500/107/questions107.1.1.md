Mit welchem Befehl lege ich ein neues Konto für den User michael an?

useradd michael


Mit welchem Befehl setzt man ein Passwort für den User michael?

passwd michael


Mit welchem Befehl prüft man die UID und die GID des Users michael?

id michael


Mit welchem Befehl prüft man, in welchen Gruppen der Nutzer michael ist?

groups michael


Wie kann man die Shell des Nutzers Michael in tcsh ändern?

$ usermod -s /bin/tcsh michael


Wie kann man einen Kommentar (Michael User Account) in das Kommentarfeld des Nutzers Michael in der Passwort Datei eintragen?

$ usermod -c "Michael User Account" michael


Wie entfernt man einen Nutzer michael inkl. seinem Home Verzeichnis?

$ userdel -r michael


Wie kann man die Gruppe developer mit der GID 1090 erstellen?

$ groupadd -g 1090 developer


Müssen beim Erstellen eines neuen Nutzers die primäre und sekundäre Gruppe, zu der er hinzugefügt wird, bereits existieren oder erstellt der Befehl useradd diese automatisch?

Nein, sie müssen bereits existieren.


Wie nenne ich die Gruppe developer in die Gruppe web-developer um und ändere die GID in 1050?

$ groupmod -n web-developer -g 1050 developer


Wie entferne ich die Gruppe web-developer?

$ groupdel web-developer


Kann eine Gruppe entfernt werden, wenn noch ein Konto diese als primäre Gruppe hinterlegt hat, oder muss diese zuerst entfernt werden? Wie verhält es sich mit sekundären Gruppen?

Beim Entfernen einer Gruppe darf diese in keinem Konto mehr als primär hinterlegt sein.


Mit welchem Befehl kann ich alle Dateien im Skeleton Verzeichnis auflisten? Was liegt dort standardmäßig für Dateien?

$ ls -la /etc/skel
total 28
drwxr-xr-x   2 root root  4096 Sep 10  2020 .
drwxr-xr-x 117 root root 12288 Mär 30 08:14 ..
-rw-r--r--   1 root root   220 Apr  4  2018 .bash_logout
-rw-r--r--   1 root root  3771 Apr  4  2018 .bashrc
-rw-r--r--   1 root root   807 Apr  4  2018 .profile


In welcher Datei sind Standardparameter für das Erstellen von Benutzerkonten, wie z.B. UID_MIN, UID_MAX, GID_MIN und GID_MAX hinterlegt?

/etc/login.defs


Wer kann den Befehl passwd aufrufen?
$ ls -l /usr/bin/passwd
-rwsr-xr-x 1 root root 42096 mag 17  2015 /usr/bin/passwd

Das SUID Bit ist gesetzt. Dadurch kann führt jeder Nutzer den Befehl mit den Rechten des Nutzers root aus. Dies ist notwendig, damit passwd auch die Einträge in den Dateien /etc/passwd, /etc/shadow aktualisieren kann.


Mit welchem Befehl legt man ein Passwort für eine Gruppe fest?

gpasswd
Was macht dieser Befehl?

Mit welchem Befehl legt man ein Passwort für eine Gruppe fest?


Was bewirkt der Befehl chage und welche Option können normale Nutzer nur benutzen?

Er existiert, um die Informationen zum Ablauf des Passwortes anzuzeigen und zu ändern.
Die Option -l kann lediglich von normalen Usern benutzt werden.
Wie heißt der Command?

chage


# Lösungen zu den geführten Übungen

Geben Sie für jeden der folgenden Befehle den entsprechenden Zweck an:

usermod -L  Sperren des Benutzerkontos
passwd -u   Entsperren des Benutzerkontos
chage -E    Verfallsdatum für das Benutzerkonto festlegen
groupdel    Löschen der Gruppe
useradd -s  Anlegen eines neuen Benutzerkontos mit einer bestimmten Login-Shell
groupadd -g Anlegen einer neuen Gruppe mit einer bestimmten GID
userdel -r  Entfernen eines Benutzerkontos und aller Dateien in dessen Homeverzeichnis, des Homeverzeichnisses selbst sowie des Mail-Spool des Benutzers
usermod -l  Ändern des Anmeldenamens des Benutzerkontos
groupmod -n Namensänderung einer Gruppe
useradd -m  Anlegen eines neuen Benutzerkontos und dessen Homeverzeichnisses


Geben Sie für jeden der folgenden passwd-Befehle den entsprechenden chage-Befehl an:
Erläutern Sie den jeweiligen Zweck der Befehle in der vorherigen Frage.

passwd -n   chage -m
Unter Linux können Sie mit dem Befehl passwd -n (oder chage -m) die minimale Anzahl von Tagen zwischen Passwortänderungen festlegen.
passwd -x   chage -M
Mit dem Befehl passwd -x (oder chage -M)die maximale Anzahl von Tagen, die ein Passwort gültig ist.
passwd -w   chage -W
Der Befehl passwd -w (oder chage -W) legt die Anzahl von Tagen fest, die ein Benutzer vor Ablauf des Passwort gewarnt wird.
passwd -i   chage -I
Mit passwd -i (oder chage -I) bestimmen Sie die Anzahl von Tagen der Inaktivität, in denen der Benutzer das Passwort ändern muss.
passwd -S   chage -l
passwd -S (oder chage -l) zeigt Informationen über das Passwort des Benutzerkontos an.   


Mit welchen Befehlen können Sie ein Benutzerkonto sperren? Und mit welchen Befehlen, können Sie es wieder entsperren?

Um ein Benutzerkonto zu sperren, nutzen Sie einen der folgenden Befehle: usermod -L, usermod --lock oder passwd -l.
Um es zu entsperren, wählen Sie usermod -U, usermod --unlock oder passwd -u.

# Lösungen zu den offenen Übungen

Erstellen Sie als root mit dem Befehl groupadd die Gruppen administrators und developers.

$ groupadd administrators
$ groupadd developers


Nachdem Sie nun diese Gruppen administrators und developers erstellt haben, führen Sie den folgenden Befehl aus: useradd -G administrators,developers kevin. Welche Operationen führt dieser Befehl aus? Nehmen Sie an, dass CREATE_HOME und USERGROUPS_ENAB in /etc/login.defs auf yes gesetzt sind.

Der Befehl fügt einen neuen Benutzer namens kevin zur Liste der Benutzer im System hinzu, erstellt sein Homeverzeichnis (CREATE_HOME ist auf yes gesetzt und daher können Sie die Option -m vernachlässigen) und erstellt eine neue Gruppe namens kevin als primäre Gruppe dieses Benutzerkontos (USERGROUPS_ENAB ist ebenfalls auf yes gesetzt). Schließlich werden die im Skeleton-Verzeichnis enthaltenen Dateien und Ordner in das Homeverzeichnis von kevin kopiert.


Erstellen Sie eine neue Gruppe namens designers, benennen Sie diese in web-designers um und fügen Sie diese neue Gruppe zu den sekundären Gruppen des Benutzerkontos kevin hinzu. Identifizieren Sie alle Gruppen, zu denen kevin gehört, und deren IDs.

# groupadd designers
# groupmod -n web-designers designers
# usermod -a -G web-designers kevin
# id kevin
uid=1010(kevin) gid=1030(kevin) groups=1030(kevin),1028(administrators),1029(developers),1031(web-designers)

# id kevin
uid=1010(kevin) gid=1030(kevin) groups=1030(kevin),1028(administrators),1029(developers),1031(web-designers)
Entfernen Sie nur die Gruppe developers aus den sekundären Gruppen von kevin.

# usermod -G administrators,web-designers kevin
# id kevin
uid=1010(kevin) gid=1030(kevin) groups=1030(kevin),1028(administrators),1031(web-designers)
Der Befehl usermod verfügt über keine Option zum Entfernen nur einer Gruppe; daher müssen Sie alle sekundären Gruppen angeben, zu denen der Benutzer gehört.


Legen Sie das Passwort für das Benutzerkonto kevin fest.

# passwd kevin
Changing password for user kevin.
New UNIX password:
Retype new UNIX password:
passwd: all authentication tokens updated successfully.


Überprüfen Sie mit dem Befehl chage zunächst das Ablaufdatum des Benutzerkontos kevin und ändern Sie es dann auf den 31. Dezember 2022. Welchen anderen Befehl können Sie zum Ändern des Ablaufdatums eines Benutzerkontos verwenden?

# chage -l kevin | grep "Account expires"
Account expires		: never
# chage -E 2022-12-31 kevin
# chage -l kevin | grep "Account expires"
Account expires		: dec 31, 2022
Der Befehl usermod mit der Option -e ist äquivalent zu chage -E.


Fügen Sie ein neues Benutzerkonto namens emma mit der UID 1050 hinzu und legen Sie administrators als primäre Gruppe und developers und web-designers als sekundäre Gruppen fest.

# useradd -u 1050 -g administrators -G developers,web-designers emma
# id emma
uid=1050(emma) gid=1028(administrators) groups=1028(administrators),1029(developers),1031(web-designers)


Ändern Sie die Login-Shell von emma in /bin/sh.

# usermod -s /bin/sh emma


Löschen Sie die Benutzerkonten emma und kevin sowie die Gruppen administrators, developers und web-designers.

# userdel -r emma
# userdel -r kevin
# groupdel administrators
# groupdel developers
# groupdel web-designers


# Extra

Was bedeutet PASS_MAX_DAYS in /etc/login.defs?

Die Anzahl von Tagen, in der das Passworts geändert werden muss. Es ist auch das Auslaufsdatum des Passwortes.


Was bedeutet PASS_MIN_DAYS in /etc/login.defs?

Die Anzahl von Tagen, die mindestens vergangen sein muss, bevor es erneut geändert wird.


Was bedeutet PASS_MIN_LENGTH in /etc/login.defs?

Die minimale Anzahl von Zeichen, die ein PW enthalten muss.


Was bedeutet PASS_WARN_AGE in /etc/login.defs?

Die Anzahl an Tagen bis eine Warnung vor dem Auslaufen des PW ausgegeben wird.


Was bedeutet CREATE_HOME in /etc/login.defs?

Wenn es auf yes gesetzt ist, wird beim Anlegen ein Home Ordner für den User angelegt.


Was bedeutet ENCRYPT_METHOD in /etc/login.defs?

Es beschreibt die genutze Hash Methode für die Speicherung von PWs.