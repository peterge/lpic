Mon Oct 21 10:45:21 -03 2019
Welcher Befehl erzeugt dieses Output?
Welche Zeitzone wird vom System verwendet?

$ date
GMT-3, also 3 hinter UTC.


Wie heißt ein Befehl unter systemd, um sich die Informationen zur Systemzeit und zum Datum anzuzeigen?

timedatectl


In welcher Datei die Zeitzone definiert, die das System verwendet? Wie lautet diese?

$ cat /etc/timezone
Europe/Berlin


Wie kann man sich den korrekten Namen einer Zeitzone anzeigen lassen?

tzselect


Welche Umgebungsvariable wird genutzt um die Zeitzone anzugeben?
Was trägt man in ~/.profile ein, um America/Sao_Paulo für alle zukünftigen Sitzungen zu speichern?

TZ
TZ='America/Sao_Paulo'; export TZ


Wie kann ich mir die aktuelle Uhrzeit für Africa/Cairo anzeigen lassen?

$ TZ='Africa/Cairo' date
Mon Oct 21 15:45:21 EET 2019


Die Datei /etc/timezone ist nur ein symbolischer Link. Worauf zeigt er?

/usr/share/zoneinfo/Europe/Berlin


Was enthält LANG bei einem System mit englischer Sprache und dem Regionalcode der USA, welches die Unicode-Zeichenkodierung für westliche Zeichen verwendet?

en_US.UTF-8


Mit welchem Befehl kann ich auf systemd Systemen die Gebietsschemakonfiguration anpassen?

localectl set-locale LANG=de_US.UTF-8


In welcher Datei stehen systemweite Locale-Einstellungen?

$ cat /etc/locale.conf
LANG=pt_BR.UTF-8


Wie kann ich die aktuelle Gebietsschemakonfiguration anzeigen lassen?

locale


Wie heißt die Variable, die in der Lage ist alle anderen Gebietsschemaeinstellungen außer Kraft zu setzen und daher den Wert "" hat?

LC_ALL
Was macht diese Variable? Welchen Wert hat sie standardmäßig?

Sie überschreibt alle anderen Gebietsschemaeinstellungen und hat daher den Wert "".


Mit welchem Command konvertiere ich eine Datei in eine andere mit einer unterschiedlichen Zeichenkodierung?

iconv


# Lösungen zu den geführten Übungen

Basierend auf der folgenden Ausgabe des Befehls date, was ist die Zeitzone des Systems in GMT-Notation?
date
Mon Oct 21 18:45:21 +05 2019

Es handelt sich um die Zeitzone Etc/GMT+5.


Auf welche Datei sollte der symbolische Link /etc/localtime zeigen, um Europe/Brussels zur Standardortszeit des Systems zu machen?

Der Link /etc/localtime sollte auf /usr/share/zoneinfo/Europe/Brussels zeigen.

# Lösungen zu den offenen Übungen

Welcher Befehl macht Pacific/Auckland zur Standardzeitzone für die aktuelle Shellsitzung?

export TZ=Pacific/Auckland


Der Befehl uptime zeigt u.a. den Lastdurchschnitt des Systems in Kommazahlen an. Er verwendet die aktuellen Gebietsschemaeinstellungen, um zu entscheiden, ob das Dezimaltrennzeichen ein Punkt oder ein Komma ist. Wenn zum Beispiel das aktuelle Gebietsschema auf de_DE.UTF-8 eingestellt ist, wird uptime ein Komma als Trennzeichen setzen.
Welcher Befehl bewirkt, dass uptime die Werte für den Rest der aktuellen Sitzung mit einem Punkt statt einem Komma als Trennzeichen darstellt?

Der Befehl export LC_NUMERIC=en_US.UTF-8 oder export LC_ALL=en_US.UTF-8.


Zeichen in Textdateien werden auf einem System mit einer anderen Zeichenkodierung als der im Textdokument verwendeten möglicherweise nicht korrekt wiedergegeben. Wie nutzen Sie iconv, um die WINDOWS-1252-kodierte Datei old.txt in die Datei new.txt mit UTF-8-Kodierung zu konvertieren?

Der Befehl iconv -f WINDOWS-1252 -t UTF-8 -o new.txt old.txt führt die gewünschte Konvertierung durch.
-f: From
-t: To
-o: Output

Wie konvertiert man eine Datei original.txt von der ISO-8859-1-Kodierung in die Datei mit dem Namen converted.txt mit UTF-8-Kodierung?

iconv -f ISO-8859-1 -t UTF-8 original.txt > converted.txt


Wie kann man sich alle vom Befehl iconv unterstützten Kodierungen anzeigen lassen?

iconv -l