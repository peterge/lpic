Wie kann man auf einem Linux System einmalige Aufgaben planen?

Mit at
Was macht es?

Einmalige Aufgaben planen.


Wie lässt sich der Befehl date in 5 minuten ausführen?

at now +5 minutes
> date
> Ctrl+D


Welcher Deamon muss im System laufen damit das Command Schduling mit at funktioniert?

atd


Wie heißt ein ähnliches Programm zu at, das jedoch nur läuft wenn die Systemlast (Load < 1.5) niedrig ist?

batch
Was macht das Programm?

Wie at, jedoch nur bei niedriger Load (< 1.5).


Wie kann man sich alle mit at erstellten Jobs anzeigen lassen?

atq
at -l


Wie kann man das Skript foo.sh um 9:30 ausführen?

$ at 09:30 AM
warning: commands will be executed using /bin/sh
at> ./foo.sh
at> Ctrl+D
job 13 at Sat Sep 14 09:30:00 2019


Wie kann man das Skript bar.sh in 2 Stunden ausführen?

$ at now +2 hours
warning: commands will be executed using /bin/sh
at> ./bar.sh
at> Ctrl+D
job 14 at Sat Sep 14 11:10:00 2019


➜  ~ atq
2	Tue Apr 26 10:48:00 2022 a peterge
3	Tue Apr 26 09:48:00 2022 a peterge
Wie lösche ich den obersten Job?

atrm 2


Wie wird gesteuert ob ein Benutzer at verwenden darf oder nicht?

Durch /etc/at.allow und /etc/at.deny.


Wie erlaubt man allen Nutzern at zu verwenden?

Durch eine leere /etc/at.deny Datei.
Wer darf at verwenden?

Alle Nutzer.


Wie erlaubt man nur dem Nutzer tom at zu verwenden?

Durch tom in /etc/at.allow.
Was bewirkt dies?

Nur der Nutzer tom darf at verwenden.


Wie heißt die Alternative zu at unter systemd?

systemd-run


Wie führe ich mit systemd-run den Befehl date um 11:30 am 6.10.2019 aus?

systemd-run --on-calendar='2019-10-06 11:30' date


Wenn Sie das Skript foo.sh, das sich in Ihrem aktuellen Arbeitsverzeichnis befindet, nach zwei Minuten ausführen möchten, geben Sie Folgendes ein?
Mit systemd-run!

systemd-run --on-active="2m" ./foo.sh


# Lösungen zu den geführten Übungen

Geben Sie für jede der folgenden Zeitangaben an, welche für at gültig und welche ungültig ist:
at 08:30 AM next week
at midday

gültig
ungültig


Geben Sie für jede der folgenden Zeitangaben an, welche für at gültig und welche ungültig ist:
at 01-01-2020 07:30 PM
at 21:50 01.01.20

ungültig
gültig


Geben Sie für jede der folgenden Zeitangaben an, welche für at gültig und welche ungültig ist:
at now +4 days
at 10:15 PM 31/03/2021
at tomorrow 08:30 AM monotonic

gültig
ungültig
ungültig


Wie überprüfen Sie die Befehle eines Jobs, den Sie mit at geplant haben?

Mit dem Befehl at -c, gefolgt von der Job-ID, überprüfen Sie dessen Befehle. Beachten Sie, dass die Ausgabe auch den größten Teil der Umgebung enthält, die zu dem Zeitpunkt aktiv war, als der Auftrag geplant wurde.


Mit welchen Befehlen überprüfen Sie Ihre ausstehenden at-Jobs? Mit welchen Befehlen würden Sie diese löschen?

Mit dem Befehl at -l oder atq überprüfen Sie Ihre ausstehenden Aufträge — mit at -d oder atrm löschen Sie sie.


Welcher Befehl ist bei systemd die Alternative zu at?

Der Befehl systemd-run ist eine Alternative zu at, um einmalige Jobs zu planen. Sie können damit z.B. Befehle zu einem bestimmten Zeitpunkt ausführen, indem Sie einen Calendar Timer oder einen Monotonic Timer relativ zu verschiedenen Startpunkten definieren.

# Lösungen zu den offenen Übungen

Erstellen Sie einen at-Job, der das Skript foo.sh in Ihrem Homeverzeichnis am nächsten 31. Oktober um 10:30 Uhr ausführt. Sie arbeiten als normaler Benutzer.

$ at 10:30 AM October 31
warning: commands will be executed using /bin/sh
at> ./foo.sh
at> Ctrl+D
job 50 at Thu Oct 31 10:30:00 2019


Melden Sie sich als ein anderer normaler Benutzer am System an und erstellen Sie einen weiteren at-Job, der das Skript bar.sh morgen um 10:00 Uhr ausführt. Das Skript befindet sich im Homeverzeichnis des Benutzers.

$ at 10:00 tomorrow
warning: commands will be executed using /bin/sh
at> ./bar.sh
at> Ctrl+D
job 51 at Sun Oct 6 10:00:00 2019


Melden Sie sich als ein anderer normaler Benutzer am System an und erstellen Sie einen weiteren at-Job, der das Skript foobar.sh genau nach 30 Minuten ausführt. Das Skript dazu befindet sich ebenfalls im Homeverzeichnis des Benutzers.

$ at now +30 minutes
warning: commands will be executed using /bin/sh
at> ./foobar.sh
at> Ctrl+D
job 52 at Sat Oct 5 10:19:00 2019


Führen Sie nun als root den Befehl atq aus, um die geplanten at-Jobs aller Benutzer zu überprüfen. Was passiert, wenn ein normaler Benutzer diesen Befehl ausführt?

# atq
52      Sat Oct  5 10:19:00 2019 a dave
50      Thu Oct 31 10:30:00 2019 a frank
51      Sun Oct  6 10:00:00 2019 a emma
Wenn Sie den Befehl atq als root ausführen, werden alle anstehenden at-Aufträge aller Benutzer aufgelistet. Wenn Sie ihn als normaler Benutzer ausführen, werden nur Ihre eigenen anstehenden at-Aufträge aufgelistet.


52      Sat Oct  5 10:19:00 2019 a dave
50      Thu Oct 31 10:30:00 2019 a frank
51      Sun Oct  6 10:00:00 2019 a emma
Löschen Sie als root alle ausstehenden at-Jobs mit einem einzigen Befehl.

# atrm 50 51 52


Führen Sie als Root den Befehl ls -l /usr/bin/at aus und prüfen Sie die Berechtigungen.
# ls -l /usr/bin/at
-rwsr-sr-x 1 daemon daemon 43762 Dec  1  2015 /usr/bin/at

In dieser Distribution hat der at-Befehl sowohl das SUID- (das s anstelle des Ausführungsflags für den Eigentümer) als auch das SGID-Bit (das s anstelle des Ausführungsflags für die Gruppe) gesetzt, was bedeutet, dass er mit den Rechten des Eigentümers und der Gruppe der Datei ausgeführt wird (daemon für beide). Aus diesem Grund können normale Benutzer Aufträge mit at planen.