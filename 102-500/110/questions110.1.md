Was bedeutet -rws------ und was -rwS------ in den Berechtigungen einer Datei?
Wie ist der numerische Wert der Berechtigung?

Das s/S kennzeichnet das SUID Bit, welches gesetzt ist.
-rws------ ist 4700, also mit der zugrundeliegenden Ausführungsberechtigung
-rwS------ ist 4600, also ohne die zugrundeliegenden Ausführungsberechtigung


Was bedeutet ein SUID/SGID Bit auf einer Datei?

Das alle Nutzer diese Datei mit den Rechten des Users/der Gruppe Bearbeiten/Lesen/Ausführen können.


Was bedeutet ein SUID/SGID Bit auf einem Ordner?

Das alle Nutzer die Dateien in diesem Ordner mit den Rechten des Users/der Gruppe Bearbeiten/Lesen/Ausführen können.


Wie liste ich mit find alle Dateien im aktuellen Verzeichnis auf, die exakt die Berechtigung 755 haben?
Gebe den numerischen Wert und den symbolischen Wert an!

find . -perm 755
find . -perm u+rwx,g+rx,o+rx


Wie liste ich mit find alle Dateien im aktuellen Verzeichnis auf, bei denen NUR die Schreibberechtigung für die Gruppe gesetzt ist? Alle anderen Berechtigungen sind egal.
Gebe den numerischen Wert und den symbolischen Wert an!

find . -perm -0040
find . -perm -g+r

Wie liste ich mit find alle Dateien im aktuellen Verzeichnis auf, bei denen entweder das SUID oder das SGID Bit gesetzt ist?
Gebe den numerischen Wert und den symbolischen Wert an!

find . -perm /6000
find . -perm /u+s,g+s


Mit welchem Befehl kann man sich Informationen zur Kennwortalterung anzeigen lassen?
root P 04/05/2019 0 99999 7 -1
Erkläre was P/L/NP in der zweiten Spalte bedeutet!
Erkläre was die -1 in der letzten Spalte bedeutet!

root@blog:~# passwd -S
root P 04/05/2019 0 99999 7 -1
root Benutzername
     P  gültiges Passwort
     L  gesperrtes Passwort
     NP kein Passwort
       04/05/2019 letzte Passwortänderung
                  0 minimales Alter
                    99999 maximales Alter
                          7 Warning
                            -1 Inaktivitätszeitraum (-1 = keine)


Wie kann ich mit dem Befehl passwd ein Konto sperren/entsperren/das Passwort auslaufen lassen/löschen?

passwd -l
passwd -u
passwd -e
passwd -d


Wie erhält man eine Liste aller offenen Internet-Dateien (= Ports)?

lsof -i
list open files


Was zeigt dieser Command an?
➜  ~ lsof -i@blog.peterge.de
COMMAND    PID    USER   FD   TYPE  DEVICE SIZE/OFF NODE NAME
ssh     256579 peterge    3u  IPv4 2390965      0t0  TCP peterge-pc:51904->vps.gerhards.local:EtherNet/IP-1 (ESTABLISHED)

Alle Internetverbindungen zum Host blog.peterge.de. Hier besteht eine SSH Verbindung zum angegebenen Host.


Was zeigt dieser Command an?
root@blog:~# lsof -i :22
COMMAND     PID USER   FD   TYPE     DEVICE SIZE/OFF NODE NAME
sshd        611 root    3u  IPv4      23227      0t0  TCP *:22 (LISTEN)
sshd        611 root    4u  IPv6      23238      0t0  TCP *:22 (LISTEN)
sshd    1278127 root    4u  IPv4 2368162324      0t0  TCP v220211259900171827.nicesrv.de:22->p549a7af0.dip0.t-ipconnect.de:51904 (ESTABLISHED)

Alle offenen Verbindungen, gefiltert nach dem Port 22.


Was zeigt dieser Command an?
root@blog:~# lsof -i4:22,443
COMMAND     PID     USER   FD   TYPE     DEVICE SIZE/OFF NODE NAME
sshd        611     root    3u  IPv4      23227      0t0  TCP *:22 (LISTEN)
nginx       692     root    7u  IPv4      24253      0t0  TCP *:https (LISTEN)
sshd    1278127     root    4u  IPv4 2368162324      0t0  TCP v220211259900171827.nicesrv.de:22->p549a7af0.dip0.t-ipconnect.de:51904 (ESTABLISHED)
nginx   4023834 www-data    7u  IPv4      24253      0t0  TCP *:https (LISTEN)
nginx   4023835 www-data    7u  IPv4      24253      0t0  TCP *:https (LISTEN)

Alle offenen IPv4 Verbindungen zum Port 22 und 443.


Was zeigt dieser Command an?
root@blog:~# fuser -v .
                     USER        PID ACCESS COMMAND
/root:               root      1278218 ..c.. bash
                     root      1289956 ..c.. bash
                     root      1289982 ..c.. nano


fuser gibt an welcher Nutzer gerade auf welche Datei zugreift.
Im Beispiel sind 2 SSH Sitzungen geöffnet, daher zwei mal bash. In einer der beiden Sitzungen wird eine Datei mit nano bearbeitet.


Was zeigt dieser Command an?
root@blog:~# fuser -vn tcp 22
                     USER        PID ACCESS COMMAND
22/tcp:              root        611 F.... sshd
                     root      1278127 F.... sshd
                     root      1289874 F.... sshd

-v (--verbose) und -n (--namespace) gibt Information über den benutzten Netzwerk Port an, hier 22 vom Dienst SSH.

Was zeigt dieser Command an?
Wie heißt der Command für das andere Protokoll?
root@blog:~# netstat -lu
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
udp        0      0 0.0.0.0:9987            0.0.0.0:*                          
udp        0      0 v22021125990017182:3478 0.0.0.0:*                          
udp        0      0 localhost:3478          0.0.0.0:*                          
udp        0      0 v22021125990017182:3479 0.0.0.0:*                          
udp        0      0 localhost:3479          0.0.0.0:*                          
udp        0      0 localhost:domain        0.0.0.0:*                          
udp6       0      0 [::]:9987               [::]:*                             
udp6       0      0 localhost:3478          [::]:*                             

Er zeigt nur die listening/lauschenden UDP Ports an.
netstat -lt für TCP.


Wie zeigt man sich mit netstat nur die bestehenden UDP und TCP Verbindungen in einer ausführlichen Ausgabe an?

netstat -ute
u UDP
t TCP
e extend
Hierbei werden nur die bstehenden Verbindungen angezeigt! Nicht die listening/lauschenden Ports!


Wie kann man bei netstat -ute die Namen ausblenden?
root@blog:~# netstat -ute
Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       User       Inode     
tcp        0      0 blog.peterge.de:58078   172.20.0.50:2368        TIME_WAIT   root       0         
tcp        0      0 v2202112599001718:https p549a7af0.dip0.t-:10391 TIME_WAIT   root       0         
tcp6       0      0 v22021125990017182:6556 p549a7af0.dip0.t-:21836 TIME_WAIT   root       0         
udp        0      0 v2202112599001718:36384 ns3125873.ip-51-68:2010 ESTABLISHED teamspeak3 25004     
udp6       0      0 localhost:55961         localhost:55961         ESTABLISHED postgres   24619     

Der Befehl netstat -uten enhält nur numerische Informationen (--numeric).


Welche 3 Möglichkeiten gibt es, um bei nmap mehrere Hosts anzugeben?

Leerzeichen:    nmap 10.0.4.2 10.0.4.3 10.0.4.4
Bindestrich:    nmap 10.0.4.2-4
Subnetz:        nmap 10.0.4.0/24


Wie filtere ich mit nmap nur den bestimmten Port 22 des Hosts localhost?

➜  ~ nmap -p 22 localhost
Starting Nmap 7.92 ( https://nmap.org ) at 2022-05-09 10:44 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00013s latency).
Other addresses for localhost (not scanned): ::1

PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 0.03 seconds


Was macht nmap -F?

-F: Fast mode - Scan fewer ports than the default scan, die 100 wichtigsten


Mit welchem Befehl lege ich Beschränkungen für Prozesse, Logins und Speicherlimits fest? Wie heißt die Konfigdatei?

ulimit - bash bultin
/etc/security/limits.conf


Welche 3 Commands zeigen mir an, welche User gerade am System angemeldet sind?

last w who


In welcher Datei stehen diese Abschnitte?
# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL

/etc/suders


Was bewirkt diese Zeile in /etc/suders?
carol   ALL=(ALL:ALL) NOPASSWD: /usr/bin/systemctl status apache2

Der Benutzer carol darf den apache2-Status von jedem Rechner aus als beliebiger Benutzer einer beliebigen Gruppe zu überprüfen ohne dabei ein Passwort eingeben zu müssen.
carrol  ALL=(ALL:ALL) bedeutet: carol kann sich von allen Rechnern aus tuen ALL, als beliebiger Benutzer einer beliebigen Gruppe (ALL:ALL)


Wie füge ich den User carol zur Gruppe sudo hinzu?

sudo useradd -aG sudo carol
Wichtig: -a (--apend), da ich sonst nur den User carol in der sekundendären Gruppe von sudo hinterlege.


Wie heißt das Pendant zur speziellen administrativen sudo-Gruppe (Debian-Systemen) auf Systemen der RedHat-Distributionsfamilie?

wheel
https://unix.stackexchange.com/questions/1262/where-did-the-wheel-group-get-its-name/1271#1271


Wie sollte ich /etc/suders immer bearbeiten? Welcher Editor wird genutzt?

Mit dem Befehl visudo.


Wie funktionieren Host_Alias, User_Alias und Cmnd_Alias in /etc/suders?

Ich kann in /etc/sudoers bestimmte Aliase definieren, die ich später in meinen Regeln verwende:
Host_Alias SERVERS = 192.168.1.7, server1, server2
User_Alias REGULAR_USERS = john, mary, alex
User_Alias PRIVILEGED_USERS = mimi, alex
User_Alias ADMINS = carol, %sudo, PRIVILEGED_USERS, !REGULAR_USERS
Cmnd_Alias SERVICES = /usr/bin/systemctl *
[...]
root    ALL=(ALL:ALL) ALL
ADMINS  SERVERS=SERVICES


# Lösungen zu den geführten Übungen

Vervollständigen Sie die folgende Tabelle zu den Sonderrechten:
Spezielle Berechtigung 	Numerische Darstellung 	Symbolische Darstellung 	Suche nach Dateien mit nur dieser Berechtigung
SUID
SGID

SUID  4000  s,S   find -perm 4000, find -perm u+s
SGID  2000  s,S   find -perm 2000, find -perm g+s


Die Anzeige von Dateien, bei denen nur das SUID- oder SGID-Bit gesetzt ist, ist normalerweise nicht sehr praktisch. Zeigen Sie in den folgenden Aufgaben, dass Ihre Suche produktiver sein kann:
Finden Sie alle Dateien mit gesetzter SUID (und anderen Berechtigungen) in /usr/bin:

find /usr/bin -perm -4000 oder find /usr/bin -perm -u+s


Die Anzeige von Dateien, bei denen nur das SUID- oder SGID-Bit gesetzt ist, ist normalerweise nicht sehr praktisch. Zeigen Sie in den folgenden Aufgaben, dass Ihre Suche produktiver sein kann:
Finden Sie alle Dateien mit gesetzter SGID (und anderen Rechten) in /usr/bin:

find /usr/bin -perm -2000 oder find /usr/bin -perm -g+s


Die Anzeige von Dateien, bei denen nur das SUID- oder SGID-Bit gesetzt ist, ist normalerweise nicht sehr praktisch. Zeigen Sie in den folgenden Aufgaben, dass Ihre Suche produktiver sein kann:
Finden Sie alle Dateien mit gesetzter SUID oder SGID in /usr/bin:

find /usr/bin -perm /6000


Mit chage ändern Sie die Informationen zum Ablauf des Passworts eines Benutzers. Vervollständigen Sie als root die folgende Tabelle, indem Sie die richtigen Befehle für den Benutzer mary eingeben:
Bedeutung 	chage-Befehle
Passwort ist 365 Tage lang gültig
Benutzer auffordern, das Passwort bei der nächsten Anmeldung zu ändern
Mindestanzahl von Tagen zwischen Passwortänderungen ist 1

chage -M 365 mary
chage -d 0 mary
chage -m 1 mary


Mit chage ändern Sie die Informationen zum Ablauf des Passworts eines Benutzers. Vervollständigen Sie als root die folgende Tabelle, indem Sie die richtigen Befehle für den Benutzer mary eingeben:
Bedeutung 	chage-Befehle
Ablauf des Passworts deaktivieren
Benutzer ermöglichen, sein Passwort jederzeit zu ändern
Warnfrist auf 7 Tage und das Ablaufdatum des Kontos auf den 20. August 2050 setzen
Informationen zum Ablauf des aktuellen Passworts des Benutzers anzeigen

chage -M 99999 mary
chage -m 0 mary
chage -W 7 -E 2050-08-20 mary
chage -l mary


Füllen Sie die folgende Tabelle mit dem entsprechenden Netzwerkdienstprogramm aus:
Aktion 	Befehl(e)
Netzwerkdateien für den Host 192.168.1.55 auf Port 22 mit lsof anzeigen
Prozesse, die auf den Standardport des Apache-Webservers auf Ihrem Rechner zugrefen, mit fuser anzeigen
Alle UDP Sockets auf Ihrem Rechner mit netstat anzeigen
Ports 80 bis 443 auf dem Host 192.168.1.55 mit nmap scannen

lsof -i@192.168.1.55:22
fuser -vn tcp 80, fuser --verbose --namespace tcp 80
netstat -lu,netstat --listening --udp
nmap -p 80-443 192.168.1.55


Führen Sie die folgenden Aufgaben in Bezug auf Resident Set Size (RSS) und ulimit als normaler Benutzer durch:
Zeigen Sie weiche Limits für die maximale RSS:

ulimit -m, ulimit -Sm


Führen Sie die folgenden Aufgaben in Bezug auf Resident Set Size (RSS) und ulimit als normaler Benutzer durch:
Zeigen Sie harte Limits für die maximale RSS:

ulimit -Hm


Führen Sie die folgenden Aufgaben in Bezug auf Resident Set Size (RSS) und ulimit als normaler Benutzer durch:
Setzen Sie weiche Limits für die maximale RSS auf 5.000 Kilobyte:

ulimit -Sm 5000


Führen Sie die folgenden Aufgaben in Bezug auf Resident Set Size (RSS) und ulimit als normaler Benutzer durch:
Setzen Sie harte Limits für die maximale RSS auf 10.000 Kilobyte:

ulimit -Hm 10000


Führen Sie die folgenden Aufgaben in Bezug auf Resident Set Size (RSS) und ulimit als normaler Benutzer durch:
Versuchen Sie, das harte Limit für die maximale RSS auf 15.000 Kilobyte zu erhöhen. Ist das möglich? Warum?

Nein. Einmal gesetzt, können reguläre Benutzer keine harten Limits erhöhen.


Betrachten Sie die folgende Ausgabezeile des Befehls last und beantworten Sie folgenden Fragen:
carol    pts/0        192.168.1.4      Sun May 31 14:16 - 14:22  (00:06)
Wurde carol von einem entfernten Rechner aus verbunden? Warum?

Ja, die IP-Adresse des entfernten Hosts steht in der dritten Spalte.


Betrachten Sie die folgende Ausgabezeile des Befehls last und beantworten Sie folgenden Fragen:
carol    pts/0        192.168.1.4      Sun May 31 14:16 - 14:22  (00:06)
Wie lange dauerte die Sitzung von carol?

Sechs Minuten, wie in der letzten Spalte angegeben.


Betrachten Sie die folgende Ausgabezeile des Befehls last und beantworten Sie folgenden Fragen:
carol    pts/0        192.168.1.4      Sun May 31 14:16 - 14:22  (00:06)
War carol über ein echtes textbasiertes Terminal verbunden? Warum?

Nein, pts/0 in der zweiten Spalte zeigt an, dass die Verbindung über einen grafischen Terminalemulator (Pseudo Terminal Slave) hergestellt wurde.


Betrachten Sie den folgenden Auszug aus /etc/sudoers und beantworten Sie die folgende Frage.
# Host alias specification
Host_Alias SERVERS = 192.168.1.7, server1, server2
# User alias specification
User_Alias REGULAR_USERS = john, mary, alex
User_Alias PRIVILEGED_USERS = mimi, alex
User_Alias ADMINS = carol, %sudo, PRIVILEGED_USERS, !REGULAR_USERS
# Cmnd alias specification
Cmnd_Alias WEB_SERVER_STATUS = /usr/bin/systemctl status apache2
# User privilege specification
root    ALL=(ALL:ALL) ALL
ADMINS  SERVERS=WEB_SERVER_STATUS
# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL
Kann alex den Status des Apache-Webservers auf einem beliebigen Host überprüfen? Warum?

Nein, da er Mitglied von REGULAR_USERS ist und diese Benutzergruppe von ADMINS ausgeschlossen ist — die einzigen Benutzer (abgesehen von carol, Mitgliedern der Gruppe sudo und root), die systemctl status apache2 auf SERVERS ausführen dürfen.


# Lösungen zu den offenen Übungen

Neben SUID und SGID gibt es noch eine dritte spezielle Berechtigung: das Sticky Bit. Es wird meist für Verzeichnisse wie /tmp verwendet, um zu verhindern, dass normale Benutzer andere Dateien als ihre eigenen löschen oder verschieben. 
Setzen Sie das Sticky Bit von ~/temporal:

chmod +t temporal, chmod 1755 temporal


Neben SUID und SGID gibt es noch eine dritte spezielle Berechtigung: das Sticky Bit. Es wird meist für Verzeichnisse wie /tmp verwendet, um zu verhindern, dass normale Benutzer andere Dateien als ihre eigenen löschen oder verschieben. 
Finden Sie Verzeichnisse, die mit dem Sticky Bit (und allen anderen Berechtigungen) in Ihrem Homeverzeichnis gesetzt sind:

find ~ -perm -1000, find ~ -perm /1000


Neben SUID und SGID gibt es noch eine dritte spezielle Berechtigung: das Sticky Bit. Es wird meist für Verzeichnisse wie /tmp verwendet, um zu verhindern, dass normale Benutzer andere Dateien als ihre eigenen löschen oder verschieben. 
Entfernen Sie das Sticky Bit für ~/temporal:

chmod -t temporal, chmod 0755 temporal


Das Passwort eines Benutzers wurde durch passwd -l username oder usermod -L username gesperrt. Wie erkennen Sie dies in /etc/shadow?

Im zweiten Feld erscheint ein Ausrufezeichen, direkt nach dem Anmeldenamen des betroffenen Benutzers (z.B.: mary:!$6$gOg9xJgv…​).


Wie lautet der entsprechende usermod-Befehl zu chage -E date username oder chage --expiredate date username?

usermod -e date username, usermod --expiredate date username


Geben Sie zwei verschiedene nmap-Befehle an, um alle 65535 Ports von localhost zu scannen:

nmap -p 1-65535 localhost und nmap -p- localhost