Was passiert wenn ich zum ersten mal eine SSH Verbindung zu einem Host aufbaue? Wie heißt die Datei?

SSH kennt den Fingerprint des Hosts noch nicht und fragt deshalb ob es diesen abspeichern soll.
~/.ssh/known_hosts


Wie führe ich nur einen einzigen Befehl ls mit SSH auf ina@halof aus?

ssh ina@halof ls


Wie gebe ich das Ziel einer SSH Verbindung an?
Was ist beim ersten Teil zu beachten?

<username>@<hostname/ip>
Wenn ich mich als User peter mit dem User peter des entfernten Systems anmelden möchte, kann ich den Nutzernamen weglassen und nur den Hostnamen/IP angeben.


Was passiert wenn ein Rechner die selbe IP nimmt, wie ein Rechner zu dem ich much bereits verbunden habe? Passiert eher bei DHCP.

Eine Warnung wird angezeigt:
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


Wie lautet eine Möglichkeit alle Einträge zur IP 192.168.1.77 aus der entsprechenden Datei zu entfernen?
Um welche Datei geht es?

ssh-keygen -R 192.168.1.77
~/.ssh/known_hosts


Wie erzeuge ich einen neuen SSH Key des Typs ecdsa und der Bitlänge 512?
Wie heißen die erstellten Dateien, wenn der vorgeschlagene Name verwendet wird und kein Key existiert?

ssh-keygen -t ecdsa -b 512
id_ecdsa und
id_ecdsa.pub


Worin muss welche Zeile eingetragen werden, um mich mit einem System unter der Verwendung meines privaten Keys anzumelden?

.ssh/authorized_keys
Der öffentliche Key aus id_ecdsa.pub


Welches Problem entsteht bei der Verwendung von SSH Schlüsseldateien im Klartext? Wie geht man dagegen vor?

Mit Keys im Klartext erhält jemand, der den Schlüssel in die Finger bekommt Zugriff auf das System wo der Key hinterlegt ist.
Daher empfiehlt sich die Verwendung einer Passphrase, die den Key schützt.


Welches Programm Kombiniert Bequemlichkeit und Sicherheit von Passphrase geschützten SSH Keys?
Wie hinterlegt man einen?

ssh-agent
ssh-add


Wo liegen die Schlüsselpaare — eines für jeden unterstützten Algorithmus --, die bei der Installation des OpenSSH-Servers erzeugt werden? Wie viele Paare, wie viele Keys?

4 Paare, 8 Keys:
root@blog:/etc/ssh# ls ssh_host*
ssh_host_dsa_key        ssh_host_ed25519_key
ssh_host_dsa_key.pub    ssh_host_ed25519_key.pub
ssh_host_ecdsa_key      ssh_host_rsa_key
ssh_host_ecdsa_key.pub  ssh_host_rsa_key.pub


Wie sind die Berechtigungen der privaten Keys in /etc/ssh?
Wie sind die Berechtigungen der öffentlichen Keys in /etc/ssh?

rw------- 0600
rw-r--r-- 0644


Was macht dieser Command?
ssh -L 8585:blog.peterge.de:80 localhost
Was ist nun möglich?

Die Verbindung zu www.gnu.org per SSH über Port 8585 auf Ihrem lokalen Rechner tunneln.
Wnn man im Browser http://localhost:8585 aufruft, erscheint blog.peterge.de.


Was macht dieser Command?
ssh -L 8585:www.gnu.org:80 -Nf ina@192.168.1.77
Was bedeuten -N, -f und ina@?

Dank der Option -N haben wir uns nicht auf einem anderen Rechner eingeloggt, sondern stattdessen die Portweiterleitung durchgeführt.
Die Option f weist SSH an, im Hintergrund zu laufen.
Wir haben den Benutzer ina für die Weiterleitung angegeben: ina@192.168.1.77


Was bewirkt ssh -X ina@halof?
Was passiert wenn ich nun firefox ausführe?

X11Forwarding
Die Anzeige von Firefox startet auf meinem Rechner, aber das Programm läuft auf halof.


# Lösungen zu den geführten Übungen

Melden Sie sich als Benutzer sonya auf Ihrem Client-Rechner an und führen Sie die folgenden SSH-Aufgaben auf dem entfernten Server halof aus:
Führen Sie den Befehl aus, um den Inhalt von ~/.ssh als Benutzer serena auf dem entfernten Rechner aufzulisten. Kehren Sie dann zu Ihrem lokalen Terminal zurück.

ssh serena@halof ls .ssh


Melden Sie sich als Benutzer sonya auf Ihrem Client-Rechner an und führen Sie die folgenden SSH-Aufgaben auf dem entfernten Server halof aus:
Melden Sie sich als Benutzer serena auf dem entfernten Rechner an.

ssh serena@halof

Melden Sie sich als Benutzer sonya auf Ihrem Client-Rechner an und führen Sie die folgenden SSH-Aufgaben auf dem entfernten Server halof aus:
Melden Sie sich als Benutzer sonya auf dem entfernten Rechner an.

ssh halof


Löschen Sie alle Schlüssel, die zu halof gehören, aus Ihrer lokalen Datei ~/.ssh/known_hosts.

ssh-keygen -R halof


Erstellen Sie auf Ihrem Client-Rechner ein Schlüsselpaar ecdsa mit 256 Bit.

ssh-keygen -t ecdsa -b 256


Erstellen Sie auf Ihrem Client-Rechner ein Schlüsselpaar ed25519 mit 256 Bit.

ssh-keygen -t ed25519
ECDSA-SK, Ed25519 and Ed25519-SK keys have a fixed length and the -b flag will be ignored.


Führen Sie die folgenden Schritte in der richtigen Reihenfolge aus, um eine SSH-Verbindung mit dem SSH-Authentifizierungsagenten herzustellen:
--
//- Starten Sie auf dem Client eine neue Bash-Shell für den Authentifizierungsagenten mit ssh-agent /bin/bash.
//- Erstellen Sie auf dem Client ein Schlüsselpaar mit ssh-keygen.
//- Auf dem Client ergänzen Sie Ihren privaten Schlüssel mit ssh-add in einem sicheren Bereich des Speichers.
//- Fügen Sie den öffentlichen Schlüssel Ihres Clients in die Datei ~/.ssh/authorized_keys des Benutzers ein, mit dem Sie sich auf dem entfernten Rechner anmelden wollen.
//- Falls noch nicht vorhanden, erstellen Sie ~/.ssh für den Benutzer, mit dem Sie sich auf dem Server anmelden wollen.
//- Verbinden Sie sich mit dem entfernten Server.
--

Die richtige Reihenfolge ist:
Schritt 1: Erstellen Sie auf dem Client ein Schlüsselpaar mit ssh-keygen.
Schritt 2: Falls noch nicht vorhanden, erstellen Sie ~/.ssh für den Benutzer, mit dem Sie sich auf dem Server anmelden wollen.
Schritt 3: Fügen Sie den öffentlichen Schlüssel Ihres Clients in die Datei ~/.ssh/authorized_keys des Benutzers ein, mit dem Sie sich auf dem entfernten Rechner anmelden wollen.
Schritt 4: Starten Sie auf dem Client eine neue Bash-Shell für den Authentifizierungsagenten mit ssh-agent /bin/bash.
Schritt 5: Auf dem Client ergänzen Sie Ihren privaten Schlüssel mit ssh-add in einem sicheren Bereich des Speichers.
Schritt 6: Verbinden Sie sich mit dem entfernten Server.


Welche Option und welche Anweisung werden bei Port Forwarding für die folgenden Tunneltypen verwendet?
Tunneltyp 	Option 	Anweisung
Lokal
Remote oder Reverse
X

Lokal               -L  AllowTcpForwarding  
Remote oder Reverse -R  GatewayPorts
X                   -X  X11Forwarding


Sie geben den Befehl ssh -L 8888:localhost:80 -Nf ina@halof in das Terminal Ihres Client-Rechners ein. Auf dem Client-Rechner rufen Sie mit einem Browser http://localhost:8888 auf. Was werden Sie erhalten?

Die Homepage des Webservers von halof, wie localhost aus Sicht des Servers verstanden wird.

# Lösungen zu den offenen Übungen

Welche Anweisung setzen Sie in /etc/ssh/sshd_config, um root-Logins zu ermöglichen?

PermitRootLogin


Welche Anweisung setzen Sie in /etc/ssh/sshd_config, um nur einen lokalen Account anzugeben, der SSH-Verbindungen akzeptiert?

AllowUsers


Sie nutzen denselben Benutzer auf dem Client und dem Server. Welchen ssh-Befehl geben Sie ein, um den öffentlichen Schlüssel des Clients auf den Server zu übertragen, so dass Sie sich über die Authentifizierung mit öffentlichem Schlüssel anmelden können?

ssh-copy-id


Erstellen Sie zwei lokale Porttunnel mit einem einzigen Befehl, der die lokalen unprivilegierten Ports 8080 und 8585 über den entfernten Server halof an die Websites www.gnu.org bzw. www.melpa.org weiterleitet. Verwenden Sie den Benutzer ina auf dem entfernten Server — und vergessen Sie nicht den Schalter -Nf.

ssh -L 8080:www.gnu.org:80 -L 8585:www.melpa.org:80 -Nf ina@halof