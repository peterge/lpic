Welche Felder binhaltet die Datei /etc/passwd in welcher Reihenfolge?

Die Datei beinhaltet 7 Felder, die in der Reihenfolge angeordnet sind, wie sie beim Login abgearbeitet werden.
Benutzername > Passwort > UID > GID > Kommentar > Homeverzeichnis > Shell


Wie lasse ich das Konto mit dem Befehl chage ablaufen, wenn heute der 27.03.2020 ist?
Wie sperre ich das Konto mit passwd vorrübergehend?

sudo chage -E 2020-03-26 emma
sudo passwd -l emma


Wie verhindert man die Anmeldung eines Users mit einer Methode, die die Standardshell des Nutzers involviert?

sudo usermod -s /sbin/nologin emma


Wie deaktiviere ich SSH beim Neustart?

systemctl disable sshd.service


Was macht ein Superdeamon? Wie heißen zwei bekannte Dienste?
Wo liegt der Vorteil?

Der Superdeamon lauscht auf eingehende Anfragen und startet dann den jeweiligen Dienst.
So muss nicht jeder Dienst zu jeder Zeit laufen, um seinen Port für eingehende Anfragen zu überwachen.


Wo legt man eine Konfiguration an, für einen Dienst der vom Superdeamon überwacht werden soll? Beispiel ssh

/etc/xinetd.d/ssh


Wie heißt der Nachfolger des Superdeamons unter systemd?
Wie aktiviert man ihn?

systemd-Socket-Units
Sicherstellen, dass SSH und xinetd nicht laufen, dann sudo systemctl start ssh.socket


Wie listet man sich alle laufenden Service-Units unter systemd auf?

systemctl list-units --state active --type service


Wie stoppe und deaktiviere ich eine laufende Service-Unit unter systemd? gdm.service

sudo systemctl disable gdm.service --now
"systemctl enable", "systemctl disable" and "systemctl mask" now support a new "--now" switch. If specified the units that are enabled will also be started, and the ones disabled/masked also stopped.


Wie liste ich auf, ob die SSH Bibiothek (/usr/bin/sshd) die Abhängigkeit libwrap verwendet, die TCP Wrapper benötigt?

ldd /usr/sbin/sshd | grep "libwrap"


Was bewirkt die folgende Zeile in /etc/hosts.deny?
sshd: ALL

Per TCP Wrapper verbiete ich alle Verbindungen zum sshd Dienst.


Was bewirkt die folgende Zeile in /etc/hosts.allow?
sshd: LOCAL

Per TCP Wrapper erlaube ich lokale Verbindungen zum sshd Dienst.


# Lösungen zu den geführten Übungen

Wie schalten Sie das zuvor gesperrte Konto emma wieder frei?

Der Superuser führt passwd -u emma aus, um das Konto zu entsperren.


Das Konto emma hat aktuell ein Verfallsdatum. Wie setzen Sie das Verfallsdatum auf "`nie`?
Wie überprüfe ich das Verfallsdatum nun?

Der Superuser führt chage -E -1 emma aus, um das Verfallsdatum auf “nie” zu setzen. Diese Einstellung überprüfen Sie mit chage -l emma.


Der Dienst CUPS, der Druckaufträge verarbeitet, wird auf Ihrem Server nicht benötigt. Wie deaktivieren Sie den Dienst dauerhaft? Wie überprüfen Sie, dass der entsprechende Port nicht mehr aktiv ist?

Als Superuser folgenden Befehl ausführen:
systemctl disable cups.service --now
Nun wie folgt die Inaktivität überprüfen:
netstat -l | grep ":ipp "
ss -l | grep ":ipp "
"systemctl enable", "systemctl disable" and "systemctl mask" now support a new "--now" switch. If specified the units that are enabled will also be started, and the ones disabled/masked also stopped.


Sie haben den Webserver nginx installiert. Wie überprüfen Sie, ob nginx TCP-Wrapper unterstützt?

ldd /usr/sbin/nginx | grep "libwrap"


# Lösungen zu den offenen Übungen

Verhindert die Existenz der Datei /etc/nologin die Anmeldung des Benutzers root?

Der Benutzer root kann sich weiterhin anmelden.


Verhindert das Vorhandensein der Datei /etc/nologin passwortlose Anmeldungen mit SSH-Schlüsseln?

Ja, auch passwortlose Anmeldungen werden verhindert.


Was passiert beim Login, wenn die Datei /etc/nologin nur die Textzeile login is currently not possible enthält?

Es erscheint die Meldung login is currently not possible, und eine Anmeldung wird verhindert.


Erhält der normale Benutzer emma Informationen über den Benutzer root in der Datei /etc/passwd, z.B. mit dem Befehl grep root /etc/passwd?

Ja, denn alle Benutzer haben Leserechte für diese Datei.


Erhält der normale Benutzer emma Informationen über sein eigenes gehashtes Passwort in der Datei /etc/shadow, z.B. mit dem Befehl grep emma /etc/shadow?

Nein, denn normale Benutzer haben keine Leseberechtigung für diese Datei.