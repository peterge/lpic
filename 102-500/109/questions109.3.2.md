Wie heißt das Programm zur Verfolgung der Route eines Pakets?
Wie für IPv6?

traceroute/traceroute6


Wie arbeitet traceroute/6 genau?

Es sendet mehrere Pakete ans Ziel, die über eine TTL von 1-30 verfügen.
Beginnend bei einer TTL von 1, erreicht es nur den ersten Hop, bis dieser eine "Packet expired" Meldung zurücksendet. Dann wird die TTL um 1 erhöht. So "scannt" traceroute alle Hops, die ein Paket zum Ziel nimmt und zeigt diese mit den benötigten Zeiten an.


Was bedeutet ein * in der Ausgabe von traceroute?

Das das Paket "TTL expired" nie zurück erhalten wurde. Dies bedeutet, das der letzte Hop der angezeigt wird der letzte Hop der Route ist.


Welche Art von Anfragen versendet traceroute standardmäßig?

UDP


Was macht traceroute -I?

ICMP Anfragen statt UDP Anfragen verwenden.


Welche Paketart ist bei traceroute der Standard?

Windows: ICMP
Unix: UDP


Warum ist mit traceroute die Verwendung von ICMP Paketen oft sinnvoller und efektiver als mit UDP?

Die Hops antworten mit einer höheren Warscheinlichkeit auf ICMP Pakete als auf UDP.


Wie führe ich traceroute mit höchstens 60 Hops und TCP auf Port 80 aus?

traceroute -m 60 -T -p 80


Wie funktioniert tracepath?
Wozu dient es?

Wie traceroute, nur dient es dazu die kleinste MTU auf der Route zum Zielhost zu ermitteln.


Was nimmt man statt tracepath unter IPv6?

tracepath6


Wie kann man mit netcat einen Listener auf Port 1234 erstellen?

nc -l 1234


Wie sendet netcat standardmäßig? Wie verwendet man das andere Protokoll?

TCP, nc -u für UDP


Wozu dient die Option nc -e? Achtung! Nicht in jeder Installation von netcat verfügbar!

Alles, was es empfängt, an die Standardeingabe der nachfolgenden ausführbaren Datei zu senden. Z.B. /bin/bash.


Mit welchen beiden Programmen kann ich aktuelle Listener und Verbindungen anzeigen?
Welches ist hier das Legacy Werkzeug?

ss und netstat
netstat ist alt.


Was ist ein häufig verwendeter Befehl, zum anzeigen der TCP und UDP Verbindungen, der lauschenden Sockel, der verbundenen Prozesse und unterdrückten Namen mit netstat/ss?

➜  ~ netstat -tulpn
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:57621           0.0.0.0:*               LISTEN      9121/spotify --forc 
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:25639         0.0.0.0:*               LISTEN      8399/./ts3client_li 
tcp        0      0 0.0.0.0:5355            0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:54081           0.0.0.0:*               LISTEN      9121/spotify --forc 
ss -tulpn


Erkläre die einzelnen Optionen von netstat/ss -tulpn!

-t  TCP
-u  UDP
-l  Listening ports
-p  Prozesses
-n  numerical addresses instead of names


# Lösungen zu den geführten Übungen

Mit welchem Befehl/welchen Befehlen senden Sie ein ICMP-Echo an learning.lpi.org?

Mit ping oder ping6:
$ ping learning.lpi.org
oder
$ ping6 learning.lpi.org


Wie ermitteln Sie die Route zu 8.8.8.8?

Mit tracepath oder traceroute.
$ tracepath 8.8.8.8
oder
$ traceroute 8.8.8.8


Welcher Befehl zeigt Ihnen, ob ein Prozess an TCP-Port 80 lauscht?

Mit ss:
$ ss -ln | grep ":80"
Mit netstat:
$ netstat -ln | grep ":80"


Wie ermitteln Sie, welcher Prozess an einem Port lauscht?

Auch hier gibt es mehrere Möglichkeiten: Mit lsof wie in der vorangegangenen Antwort, indem Sie die Portnummer ersetzen — oder mit netstat oder ss mit der Option -p. Denken Sie daran, dass netstat ein Legacy Tool ist.
# netstat -lnp | grep ":22"
Die Optionen, die mit netstat funktionieren, funktionieren auch mit ss:
# ss -lnp | grep ":22"


Wie ermitteln Sie die maximale MTU eines Netzwerkpfads?

Mit tracepath:
tracepath somehost.example.com


# Lösungen zu den offenen Übungen

Wie verwenden Sie netcat, um eine HTTP-Anfrage an einen Webserver zu senden?

Geben Sie die HTTP-Anfragezeile, alle Header und eine Leerzeile in das Terminal ein:
$ nc learning.lpi.org 80
GET /index.html HTTP/1.1
HOST: learning.lpi.org
HTTP/1.1 302 Found
Location: https://learning.lpi.org:443/index.html
Date: Wed, 27 May 2020 22:54:46 GMT
Content-Length: 5
Content-Type: text/plain; charset=utf-8
Found


Nennen Sie einige Gründe, warum das Anpingen eines Hosts fehlschlagen kann?

Es gibt eine Reihe möglicher Gründe — hier sind einige:
Der entfernte Host ist ausgefallen.
Eine ACL des Routers blockiert Ihren Ping.
Die Firewall des entfernten Hosts blockiert Ihren Ping.
Sie verwenden möglicherweise einen falschen Hostnamen oder eine falsche Adresse.
Ihre Namensauflösung gibt eine falsche Adresse zurück.
Die Netzwerkkonfiguration Ihres Rechners ist falsch.
Die Firewall Ihres Rechners blockiert Ihre Anfrage.
Die Netzwerkkonfiguration des entfernten Hosts ist falsch.
Die Schnittstelle(n) Ihres Geräts ist/sind nicht angeschlossen.
Die Schnittstelle(n) des entfernten Rechners ist/sind abgeschaltet.
Eine Netzwerkkomponente wie z.B. ein Switch, ein Kabel oder ein Router zwischen Ihrem Rechner und dem entferntem System funktioniert nicht mehr.


Nennen Sie ein Tool, mit dem Sie sehen, welche Netzwerkpakete einen Linux-Host erreichen oder verlassen?

Zum Beispiel tcpdump oder wireshark.


Wie veranlassen Sie traceroute, eine andere Schnittstelle zu verwenden?

Mit der Option -i:
$ traceroute -i eth2 learning.lpi.org
traceroute -i eth2 learning.lpi.org
traceroute to learning.lpi.org (208.94.166.201), 30 hops max, 60 byte packets
...


Ist es möglich, dass traceroute MTUs meldet?

Ja, mit der Option --mtu:
# traceroute -I --mtu  learning.lpi.org
traceroute to learning.lpi.org (208.94.166.201), 30 hops max, 65000 byte packets
1  047-132-144-001.res.spectrum.com (47.132.144.1)  9.974 ms F=1500  10.476 ms  4.743 ms
2  096-034-094-106.biz.spectrum.com (96.34.94.106)  8.697 ms  9.963 ms  10.321 ms
...