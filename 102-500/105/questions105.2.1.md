# Eigene Fragen

Wie erzeugt man mit zwei echo Befehlen eine Ausgabe in der gleichen Zeile?

echo -n erzeugt keine Newline am Ende. Also echo -n test; echo ing


Wie erzeugt man mit einem echo Befehl eine Ausgabe über zwei Zeilen?

echo 'hello\nworld' \n generiert eine neue Zeile.


Wie lautet ein Shebang für Perl /usr/bin/perl oder Python /usr/bin/python?

Perl (#!/usr/bin/perl) oder Python (#!/usr/bin/python)


Was passiert vor dem Ausführen eines Skriptes mit der Shell indem es läuft? Wie führt man es in der aktuellen Shell aus?

Es wird eine neue Subshell erzeut in der es läuft. So intereferiert es nicht mit den gesetzten Variablen/Funktionen in der eigentlichen Shell.
Es ist möglich ein Skript durch source script.sh oder . .script.sh in der aktuellen Shell laufen zu lassen.


Wie ist es möglich die Shell nach dem ausführen eines Skripts/Befehls zu beenden?

Durch den Befehl exec davor.


Wie bezieht man einen Positionsparameter größer als 9 ein?

durch ${10} oder ${11}


Wie ist es möglich in einem Skript eine Benutzerabfrage auf einem einfachen Weg in einer/mehreren Variablen abzuspeichern?

echo "Do you want to continue (y/n)?"
read ANSWER


Auf welchen 2 Wegen ist es möglich die Ausgabe des Befehls uname -o in OS zu speichern?

$ OS=`uname -o`
$ OS=$(uname -o)


Wie kann man die Anzahl der Zeichen einer Variable in einem Skript zurückgegeben bekommen?

$ echo $OS
GNU/Linux
$ echo ${#OS}
9


Auf welchen zwei Wegen ist es möglich, ein Array SIZES zu erstellen?

$ declare -a SIZES
$ SIZES=( 1048576 1073741824 )


Wie gebe ich den ersten Wert im Arrray SIZES aus?

$ echo ${SIZES[0]}
1048576


Wie gebe ich die Anzahl der Elemente im Array SIZES aus?

$ echo ${#SIZES[@]}
2
$ echo ${#SIZES[*]}
2


Wie addiert man in Bash zwei Werte?

Mit expr:
$ SUM=`expr $VAL1 + $VAL2`
Oder mit $(()):
$ SUM=$(( $VAL1 + $VAL2 ))


Wie schreibt man eine Arraydeklaration (ARRAY), die einen Potenzausdruck (1024 * 2 und 1024*3) entält?

$(())
SIZES=( $((1024**2)) $((1024**3)) )


Wie genau erkennt Bash was es bei && und || ausführen muss? Stichwort Bedingte Ausdrücke

Bei && wird der rechts stehende Ausdruck nur ausgeführt, wenn der RC des linken 0 ist.
== und
Bei || wird der rechts stehende Ausdruck nur ausgeführt, wenn der RC des linken nicht 0 ist. 
== oder


Wie schreibt man eine IF Schleife, die testet ob der Pfad /bin/bash verfügbar ist?
(2 Arten, mit test und ohne)

if test -x /bin/bash ; then
  echo "Confirmed: /bin/bash is executable."
fi
if [ -x /bin/bash ] ; then
  echo "Confirmed: /bin/bash is executable."
fi


Wie erstellt man eine IF ELSE Schleife in Bash, die prüft ob /bin/bash vorhanden ist?

if [ -x /bin/bash ] ; then
  echo "Confirmed: /bin/bash is executable."
else
  echo "No, /bin/bash is not executable."
fi


Was muss immer am Ende einer IF Schleife stehen?

fi, damit if weiß wo es zuende ist. In anderen Sprachen erledigt dies of eine Klammer ({.


Wie ist es möglich mit echo \t (horizontal tab) auszugeben?
Wie deaktiviert man dieses Verhalten?

echo -e     enable interpretation of backslash escapes)
echo -E     disable interpretation of backslash escapes (default)


Welches Programm empfiehlt sich für die Ausgabe von spezifischen Textmuster eher?

printf


# Lösungen zu den geführten Übungen

Die Option -s für den Befehl read empfiehlt sich für die Eingabe von Passwörtern, da der getippte Inhalt nicht auf dem Bildschirm erscheint. Wie setzen Sie den Befehl read ein, um die Eingaben des Benutzers in der Variablen PASSWORD zu speichern und gleichzeitig den eingetippten Inhalt zu verbergen?

read -s PASSWORD


Der einzige Zweck des Befehls whoami ist es, den Benutzernamen des Benutzers anzuzeigen, der ihn aufgerufen hat. Daher wird er meist in Skripten verwendet, um den ausführenden Benutzer zu identifizieren. Wie können Sie in einem Bash-Skript die Ausgabe des Befehls whoami in der Variablen WHO speichern? (2 M)

WHO=`whoami`
WHO=$(whoami)


Welcher Bash-Operator sollte zwischen den Befehlen apt-get dist-upgrade und systemctl reboot stehen, wenn der Root-Benutzer systemctl reboot nur in dem Fall ausführen möchte, dass apt-get dist-upgrade erfolgreich beendet wurde?

Der Operator &&, wie in apt-get dist-upgrade && systemctl reboot.


# Lösungen zu den offenen Übungen

Nach dem Versuch, ein neu erstelltes Bash-Skript auszuführen, erhalten Sie die folgende Fehlermeldung:
bash: ./script.sh: Permission denied
Sie selbst haben die Datei ./script.sh erstellt. Welche ist die wahrscheinliche Ursache für diesen Fehler?

Für die Datei ./script.sh ist die Ausführungsberechtigung nicht gesetzt.
chmod +x ./script.sh


Eine Skriptdatei namens do.sh ist ausführbar, und der symbolische Link undo.sh zeigt darauf. Wie könnten Sie aus dem Skript heraus erkennen, ob der aufrufende Dateiname do.sh oder undo.sh lautet?

Die spezielle Variable $0 enthält den Dateinamen, mit dem das Skript aufgerufen wurde.


In einem System mit einem ordnungsgemäß konfigurierten E-Mail-Dienst sendet der Befehl mail -s "Maintenance Error" root <<<"Scheduled task error" die Benachrichtigung per E-Mail an den Benutzer root. Ein solcher Befehl könnte in unbeaufsichtigten Tasks (z.B. cronjobs) dazu dienen, den Systemadministrator über ein unerwartetes Problem zu informieren. Schreiben Sie eine  einzeilige if-Anweisung, die den mail-Befehl ausführt, falls der Exit-Status des vorherigen Befehls — wie immer dieser lautet — nicht erfolgreich ist.
(IF: -not equals)

if [ "$?" -ne 0 ]; then mail -s "Maintenance Error" root <<<"Scheduled task error"; fi