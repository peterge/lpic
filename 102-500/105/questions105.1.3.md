# Eigene Fragen

Wie lege ich ein Alias ls für den Befehl ls --color=auto an?

alias ls='ls --color=auto'


Wie erstellt man 2 hintereinander ausgeführte Befehle mit einem Alias? (which git und git --version)

alias git-info='which git;git --version'


Wie erhält man eine Liste aller im System angelegten Aliase?

alias


Wie entfernt man ein Alias git-info?

unalias git-info


Was geschieht durch ein \ vor einem Alias? Wozu ist es nützlich?

Der Alias wird unwirksam. Durch das Escapen ist es möglich, einen Command aufzurufen wenn ein Alias den gleichen Namen wie ein anderer Command hat. Ein unescapter Alias hat Vorrang vor dem eigentlichen Command.


Welche 2 Dinge im Bezug auf Variablen beherrschen Aliase?

Sie können Variablen zuweisen (var=1) und ausgeben ($var).


Ist es möglich einen Alias in einem neuen Alias erneut zu verwenden?

Ja


Was ist der Unterschied zwischen alias where?='echo $PWD' und alias where?="echo $PWD"?

Wenn ein Alias mit einfachen Anführungszeichen angelegt ist, wird es dynamisch ausgeführt, es gibt also immer den aktuellen Inhalt von PWD aus. Mit " ist der Inhalt statisch.


In welcher Datei können Aliase angelegt werden und wo ist außerdem möglich?

.bashrc oder .bash_aliases


Welche 2 Struckturen zum Erstellen von Funktionen gibt es?

function name {
name() {

Wie kann man eine Funktion in der Shell anlegen?

Möglich durch PS2:
$ greet() {
> greeting="Hello world!"
> echo $greeting
> }


Wie ist es sonst möglich in der Shell eine mehrzeilige Funktion anzulegen?

Durch ;
greet() { greeting="Hello world!"; echo $greeting; }


Worauf bezieht sich die Variable $? ?

Auf den Status Code des letzten Befehls.


Worauf bezieht sich die Variable $$ ?

Auf die PID der Shell.

Worauf bezieht sich die Variable $! ?

Auf die PID des letzten Befehls.


Worauf bezieht sich die Variable $0, $1, $2, ....?

$0    gibt die aktuelle Shell aus (-bash)
$1-9 Parameter oder Argumente, die an die Funktion übergeben wurden


Worauf bezieht sich die Variable $# ?

Anzahl der übergebenen Argumente


Worauf bezieht sich die Variable $@, $* ?

Die übergebenen Argumente


Was ist nach dem einlesen einer Datei mit dieser Funktion möglich?
editors() {
editor=emacs
echo "My editor is: $editor. $editor is a fun text editor."
}

Die Variable kann mit echo $editor ausgegeben werden.


Was passiert bei einlesen einer Datei . testfunc wenn darin eine Funktion enthalten ist?

Die Funktion wird beim einlesen ein mal ausgeführt.


Welche beiden Möglichkeiten gibts es Positionsparameter in Funktionen zu verwenden?

$1 wird entweder mit den Wörtern hinter dem Namen der Funktion (test_function parameter1) gefüllt, oder beim Aufruf der Funktion (test_function parameter1).


Welche 3 Dinge muss man tuen, um eine Funktion in einer Datei in ein ausführbares Skript zu verwandeln?

Shebang #!/bin/bash
Funktion in der letzten Zeile des Skripts aufrufen
chmod +x

Wie kann ich ein Alias great_editor für die Funktion gr8_ed mit dem Befehl echo $1 is a great text editor? Was muss zudem erfolgen?

alias great_editor='gr8_ed() { echo $1 is a great text editor; unset -f gr8_ed; }; gr8_ed'


Wie setze ich eine Funktion zurück?
Wie setze ich eine Variable zurück?
Was passiert ohne Argumente?

unset -f function1
unset -v var1
Ohne Schalter versucht unset zunächst, eine Variable zurückzusetzen, und — falls dies fehlschlägt — danach eine Funktion.


# Lösungen zu den geführten Übungen

Vervollständigen Sie die Tabelle mit “Ja” oder “Nein” unter Berücksichtigung der Möglichkeiten von Aliassen und Funktionen:
Merkmal 	                                            liasse? 	Funktionen?
Lokale Variablen können verwendet werden                Ja          Ja
Umgebungsvariablen können verwendet werden              Ja          Ja


Vervollständigen Sie die Tabelle mit “Ja” oder “Nein” unter Berücksichtigung der Möglichkeiten von Aliassen und Funktionen:
Merkmal 	                                            liasse? 	Funktionen?
Kann mit \ escapet werden                               Ja          Nein
Kann rekursiv sein                                      Ja          Ja
Sehr produktiv bei Verwendung mit Positionsparametern   Nein        Ja


Wie lautet der Befehl, der alle Aliasnamen in Ihrem System auflistet?

alias


Schreiben Sie einen Alias namens logg, der alle ogg-Dateien in ~/Music auflistet — eine pro Zeile:

alias logg='ls -1 ~/Music/*ogg'


alias logg='ls -1 ~/Music/*ogg'
Ändern Sie nun den Alias so, dass er den Benutzer der Sitzung und einen Doppelpunkt vor der Auflistung ausgibt:

alias logg='echo $USER:; ls -1 ~/Music/*ogg'


Entfernen Sie den Alias logg:

unalias logg


Betrachten Sie die Spalten “Aliasname” und “Aliasbefehl(e)” und ordnen Sie die Aliasse ihren Werten korrekt zu:
Aliasname 	Aliasbefehl(e) 	                        Aliaszuordnung
b           bash                                    alias b=bash
bash_info   which bash + echo "$BASH_VERSION"       alias bash_info='which bash; echo "$BASH_VERSION"'


Betrachten Sie die Spalten “Aliasname” und “Aliasbefehl(e)” und ordnen Sie die Aliasse ihren Werten korrekt zu:
Aliasname 	Aliasbefehl(e) 	                        Aliaszuordnung
kernel_info uname -r                                alias kernel_info='uname -r'
greet       echo Hi, $USER!                         alias greet='echo Hi, $USER'
computer    pc=slimbook + echo My computer is a $pc alias computer='pc=slimbook; echo My computer is a $pc'


Schreiben Sie als root eine Funktion namens my_fun in /etc/bash.bashrc. Die Funktion muss dem Benutzer Hallo sagen und ihm mitteilen, wie sein Pfad lautet. Rufen Sie sie so auf, dass der Benutzer bei jeder Anmeldung beide Meldungen erhält:

my_fun() {
echo Hello, $USER!
echo Your path is: $PATH
}
my_fun
oder
function my_fun {
echo Hello, $USER!
echo Your path is: $PATH
}
my_fun


Schreiben Sie als root eine Funktion namens my_fun in /etc/bash.bashrc. Die Funktion muss dem Benutzer Hallo sagen und ihm mitteilen, wie sein Pfad lautet. Schreiben Sie die Funktion in nur einer Zeile:

my_fun() { echo "Hello, $USER!"; echo "Your path is: $PATH"; }
oder
function my_fun { echo "Hello, $USER!"; echo "Your path is: $PATH"; }


Rufen Sie die Funktion my_fun auf:
Setzen Sie die Funktion zurück:

my_fun
unset -f my_fun


$ special_vars2() {
> echo $#
> echo $_
> echo $1
> }
$ special_vars2 crying cockles and mussels alive alive oh
Wie lauten die Ausgaben?
Referenz 	Wert
echo $#     7
echo $_     7
echo $1     crying


$ special_vars2() {
> echo $4
> echo $6
> echo $7
> }
$ special_vars2 crying cockles and mussels alive alive oh
Wie lauten die Ausgaben?
Referenz 	Wert
echo $4     mussels
echo $6     alive
echo $7     oh


$ special_vars2() {
> echo $_
> echo $@
> echo $?
> }
$ special_vars2 crying cockles and mussels alive alive oh
Wie lauten die Ausgaben?
Referenz 	Wert
echo $_     oh
echo $@     crying cockles and mussels alive alive oh
echo $?     0


# Lösungen zu den offenen Übungen

Schreibgeschützte Funktionen sind solche, deren Inhalt wir nicht verändern können. Recherchieren Sie zu readonly functions und vervollständigen Sie die folgende Tabelle:
Funktionsname   Schreibgeschützt machen Alle schreibgeschützten Funktionen auflisten
my_fun

Funktionsname   Schreibgeschützt machen Alle schreibgeschützten Funktionen auflisten
my_fun          readonly -f my_fun      readonly -f