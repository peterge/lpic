# Lösungen zu den geführten Übungen

Stellen Sie sich vor, ein Programm muss eine temporäre Datei zur einmaligen Verwendung erstellen, die nach dem Beenden des Programms nie wieder benötigt wird. Welches wäre das richtige Verzeichnis, um diese Datei abzulegen?


Da wir uns nicht um die Datei kümmern, nachdem das Programm beendet ist, ist das richtige Verzeichnis /tmp.

Welches ist das temporäre Verzeichnis, das während des Bootvorgangs geleert werden muss?


Das Verzeichnis ist /run oder, auf manchen Systemen, /var/run.

Wie lautet der Parameter für chmod im symbolischen Modus, um das Sticky Bit für ein Verzeichnis zu aktivieren?


Das Symbol für das Sticky Bit im symbolischen Modus ist t. Da wir diese Berechtigung für das Verzeichnis aktivieren (hinzufügen) wollen, lautet der Parameter +t.

Stellen Sie sich vor, es gibt eine Datei namens document.txt im Verzeichnis /home/carol/Documents. Wie lautet der Befehl, um einen symbolischen Link darauf namens text.txt im aktuellen Verzeichnis zu erstellen?


ln -s /home/carol/Documents/document.txt text.txt

Erklären Sie den Unterschied zwischen einem harten Link zu einer Datei und einer Kopie dieser Datei.


Ein Hardlink ist nur ein anderer Name für eine Datei. Auch wenn er wie ein Duplikat der Originaldatei aussieht, sind der Link und das Original in jeder Hinsicht identisch, da sie auf dieselben Daten auf der Festplatte verweisen. Änderungen, die am Inhalt des Links vorgenommen werden, werden auf das Original übertragen und umgekehrt. Eine Kopie ist eine völlig unabhängige Einheit, die einen anderen Platz auf der Festplatte einnimmt. Änderungen an der Kopie werden nicht auf das Original übertragen und umgekehrt.

# Lösungen zu den offenen Übungen

Stellen Sie sich vor, dass Sie innerhalb eines Verzeichnisses eine Datei namens recipes.txt erstellen. Innerhalb dieses Verzeichnisses erstellen Sie auch einen harten Link zu dieser Datei, namens receitas.txt, und einen symbolischen (oder soft) Link zu dieser namens rezepte.txt.


$ touch recipes.txt
$ ln recipes.txt receitas.txt
$ ln -s receitas.txt rezepte.txt

Der Inhalt des Verzeichnisses sollte folgendermaßen aussehen:
$ ls -lhi
total 160K
5388833 -rw-r--r-- 4 carol carol 77K jun 17 17:25 receitas.txt
5388833 -rw-r--r-- 4 carol carol  0K jun 17 17:25 recipes.txt
5388837 lrwxrwxrwx 1 carol carol  12 jun 17 17:25 rezepte.txt -> receitas.txt
Denken Sie daran, dass receitas.txt als Hardlink auf denselben Inode zeigt wie rezepte.txt. Was würde mit dem Softlink rezepte.txt passieren, wenn die Datei receitas.txt gelöscht wird? Warum?


Der Softlink rezepte.txt würde nicht mehr funktionieren. Das liegt daran, dass Softlinks auf Namen und nicht auf Inodes verweisen und der Name rezepte.txt nicht mehr existiert, auch wenn die Daten noch unter dem Namen rezepte.txt auf der Festplatte vorhanden sind.

Stellen Sie sich vor, Sie haben ein Flashlaufwerk in Ihr System eingesteckt und unter /media/youruser/FlashA eingebunden. Sie möchten in Ihrem Homeverzeichnis einen Link namens schematics.pdf erstellen, der auf die Datei esquema.pdf im Stammverzeichnis des Flashlaufwerks zeigt. Sie geben also den Befehl ein:
$ ln /media/youruser/FlashA/esquema.pdf ~/schematics.pdf
Was würde passieren? Warum?

Der Befehl würde fehlschlagen. Die Fehlermeldung würde Invalid cross-device link (ungültiger geräteübergreifender Link) lauten, und macht den Grund deutlich: Harte Links können nicht auf ein Ziel in einer anderen Partition oder einem anderen Gerät zeigen. Die einzige Möglichkeit, einen solchen Link zu erstellen, ist die Verwendung eines symbolischen oder soften Links, indem Sie den Parameter -s zu ln hinzufügen.

Betrachten Sie die folgende Ausgabe von ls -lah:
$ ls -lah
total 3,1M
drwxr-xr-x 2 carol carol 4,0K jun 17 17:27 .
drwxr-xr-x 5 carol carol 4,0K jun 17 17:29 ..
-rw-rw-r-- 1 carol carol 2,8M jun 17 15:45 compressed.zip
-rw-r--r-- 4 carol carol  77K jun 17 17:25 document.txt
Wie viele Links zeigen auf die Datei document.txt?


Jede Datei beginnt mit einer Linkanzahl von 1. Da die Anzahl der Links für die Datei 4 ist, gibt es drei Links, die auf diese Datei zeigen.

Sind es symbolische oder harte Links, die auf document.txt zeigen?
-rw-r--r-- 4 carol carol  77K jun 17 17:25 document.txt


Es sind harte Links, da Softlinks die Linkanzahl einer Datei nicht erhöhen.

Welchen Parameter sollten Sie an ls übergeben, um zu sehen, welchen Inode jede Datei beansprucht?

Der Parameter ist -i. Der Inode wird als erste Spalte in der Ausgabe von ls angezeigt, wie unten zu sehen:
$ ls -lahi
total 3,1M
5388773 drwxr-xr-x 2 carol carol 4,0K jun 17 17:27 .
5245554 drwxr-xr-x 5 carol carol 4,0K jun 17 17:29 ..
5388840 -rw-rw-r-- 1 carol carol 2,8M jun 17 15:45 compressed.zip
5388833 -rw-r--r-- 4 carol carol  77K jun 17 17:25 document.txt

ln -s clients.txt partners.txt
Die Verzeichnisstruktur stellt sich also wie folgt dar:
Documents
|-- clients.txt
`-- somedir
  |-- clients.txt
  `-- partners.txt -> clients.txt
Nun verschieben Sie partners.txt von somedir nach ~/Documents und listen den Inhalt auf.
$ cd ~/Documents/
$ mv somedir/partners.txt .
$ less partners.txt
Wird der Link trotzdem funktionieren? Wenn ja, welcher Dateiinhalt wird aufgelistet? Warum?


Das ist etwas “knifflig”, aber der Link wird funktionieren, und die aufgelistete Datei werden diejenige in ~/Documents entsprechen.
Denken Sie daran, dass, da Sie beim Erstellen des Softlinks partners.txt nicht den vollständigen Pfad zum Ziel clients.txt angegeben haben. Der Zielspeicherort als relativ zum Speicherort des Links interpretiert, der in diesem Fall das aktuelle Verzeichnis ist.
Um dies zu vermeiden, geben Sie beim Erstellen eines symbolischen Links immer den absoluten Pfad der Zieldatei an.

Betrachten Sie die folgenden Dateien:
-rw-r--r-- 1 carol carol 19 Jun 24 11:12 clients.txt
lrwxrwxrwx 1 carol carol 11 Jun 24 11:13 partners.txt -> clients.txt
Wie lauten die Zugriffsrechte für partners.txt? Warum?

Die Zugriffsrechte für partners.txt entsprechen rw-r—​r--, da Links immer die gleichen Zugriffsrechte wie deren Ziel innehaben.

# Eigene

Was ist ein Hard Link?


Ein Hard Link ist ein Link zur Inode einer Datei, es können nur Inodes auf dem selben FS verlinkt werden.

Was ist ein Soft Link?


Ein Soft Link ist ein (symbolischer) Link zu einem Dateinamen, diese links können auf verschiedenen FS liegen.

Wie legt man einen Hard Link und einen symbolischen Link an?


ln <file_to_link> <hard_link_name>
ln -s <file_to_link> <soft_link_name>

Wie kann man sich die Anzahl der verlinkungen einer Datei anzeigen lassen?


ls -l die Zahl vor dem Username:
drwxrwSr-x. 2 peterge peterge      40 28. Feb 17:13 test1
-rw-r--r--. 1 peterge peterge       0 28. Feb 17:13 test2

Wieso hat ein gibt Verzeichnis standardmäßig an, dass 2 Links auf es existieren an?


Es existiert ein Link im Ordner wo es liegt und im jeweiligen Ordner selber.