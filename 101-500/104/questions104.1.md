# Lösungen zu den geführten Übungen

Welches Partitionierungsschema sollte verwendet werden, um eine 3 TB Festplatte in drei 1 GB Partitionen zu partitionieren? Warum?


GPT, da MBR höchstens 2 TB Festplatten unterstützt.

Mittels gdisk, wie können wir herausfinden, wieviel Speicherlatz auf der Platte noch verfügbar ist?


Verwenden Sie p (print). Der gesamte freie Speicherplatz wird als letzte Informationszeile vor der eigentlichen Partitionstabelle angezeigt.

Wie würde der Befehl lauten, ein ext3-Dateisystem auf dem Gerät /dev/sdc1 mit dem Label MyDisk und einer zufälligen UUID zu erstellen und vorher auf fehlerhafte Blöcke zu prüfen?


Der Befehl lautet mkfs.ext3 -c -L MyDisk -U random /dev/sdc1. Alternativ kann auch mke2fs -t ext3 anstelle von mkfs.ext3 verwendet werden.

Wie lautet der Befehl, mit parted eine 300 MB große ext4-Partition zu erstellen, beginnend bei 500 MB auf der Platte?


Verwenden Sie mkpart primary ext4 500m 800m. Denken Sie daran, dass Sie das Dateisystem mit mkfs.ext4 erstellen müssen, da parted dies nicht tut.

Stellen Sie sich vor, Sie haben 2 Partitionen, eine auf /dev/sda1 und die andere auf /dev/sdb1, beide 20 GB groß. Wie können Sie sie auf einem einzelnen Btrfs-Dateisystem so benutzen, dass der Inhalt der einen Partition automatisch auf die andere gespiegelt wird, wie bei einer RAID1-Einrichtung? Wie groß wird das Dateisystem sein?


Verwenden Sie mkfs.btrfs /dev/sda1 /dev/sdb1 -m raid1. Das resultierende Dateisystem wird 20 GB groß sein, da eine Partition einfach als Spiegel der anderen fungiert.

# Lösungen zu den offenen Übungen

Ausgehend von einer 2 GB Festplatte mit einer MBR-Partitionstabelle und dem folgenden Layout:
Disk /dev/sdb: 1.9 GiB, 1998631936 bytes, 3903578 sectors
Disk model: DataTraveler 2.0
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x31a83a48
Device     Boot   Start     End Sectors  Size Id Type
/dev/sdb1          2048 1050623 1048576  512M 83 Linux
/dev/sdb3       2099200 3147775 1048576  512M 83 Linux
Können Sie darauf eine 600 MB-Partition erstellen? Warum?


Das können Sie nicht, weil es nicht genug zusammenhängender Speicher vorhanden ist. Der erste Hinweis darauf, dass der Speicher “aus” ist, zeigt die Liste der Geräte: Sie haben /dev/sdb1 und /dev/sdb3, aber kein /dev/sdb2. Es fehlt also etwas.
Dann müssen Sie schauen, wo eine Partition endet und die andere beginnt. Die erste Partition endet im Sektor 1050623, und die zweite Partition beginnt bei 2099200. Das ist eine "Lücke" von 1048577 Sektoren. Bei 512 Bytes pro Sektor sind das 536.871.424 Bytes. Wenn man diesen Wert nun durch 1024 teilt, erhält man 524.288 Kilobyte. Erneut dividiert durch 1024, erhält man 512 MB. Dies ist die Größe der "Lücke".
Wenn die Platte 2 GB umfasst, dann stehen nach Partition 3 maximal weitere 512 MB zur Verfügung. Selbst wenn insgesamt etwa 1 GB nicht zugewieser Speicher vorliegt, beträgt der größte zusammenhängende Block 512 MB. Es gibt also keinen Platz für eine 600 MB Partition.

Auf einer Platte unter /dev/sdc haben wir die erste Partition von 1 GB, die etwa 256 MB an Dateien enthält. Wie können Sie diese Partition mit parted verkleinern, so dass sie gerade genug Platz für die abgelegten Dateien hat?
Dies ist eine mehrteilige Operation. Zuerst muss das Dateisystem mit resize2fs verkleinert werden. Anstatt die neue Größe direkt anzugeben, können Sie den -M Parameter benutzen, so dass das geänderte Dateisystem gerade “groß genug” ist. Also: resize2fs -M /dev/sdc1.


Anschließend wird die Größe der Partition selbst mit parted unter Verwendung von resizepart geändert. Da es die erste Partition ist, können wir davon ausgehen, dass diese bei Null beginnt und bei 241 MB endet. Der Befehl lautet also resizepart 1 241M.

Stellen Sie sich vor, Sie haben eine Platte unter /dev/sdb, und Sie wollen am Anfang eine 1 GB große Swap-Partition anlegen. Mit parted erstellen Sie also die Partition mittels mkpart primary linux-swap 0 1024M. Dann aktivieren Sie das Auslagern auf dieser Partition mit swapon /dev/sdb1, erhalten aber die folgende Fehlermeldung:
swapon: /dev/sdb1: read swap header failed
Was ist schief gelaufen?


Sie haben eine Partition des richtigen Typs erstellt (linux-swap), aber denken Sie daran, dass mkpart kein Dateisystem erstellt. Sie haben vergessen, die Partition zuerst mit mkswap als Auslagerungsspeicher einzurichten, bevor diese genutzt werden kann.

Stellen Sie sich vor, Sie haben eine unbenutzte 4 GB große Partition auf /dev/sda3. Mit fdisk, was wäre die Reihenfolge der Operationen, um sie in eine aktive Swap-Partition zu verwandeln?


Ändern Sie zunächst den Partitionstyp in “Linux Swap” (82), schreiben Sie Ihre Änderungen auf die Platte und beenden Sie das Programm. Dann benutzen Sie mkswap, um die Partition als Swapbereich einzurichten. Dann benutzen Sie swapon, um diesen zu aktivieren.

# Eigene

Worin liegen die Unterschiede zwischen MBR und GPT?


MBR                     
old standard            
4 primary partitions    
max size of 2TB
GPT
Associated with UEFI
Basically unlimited partitions and size

Auf welchen beiden Wegen kann man ein Dateisystem ext4 auf einer Partition erstellen?


mkfs -t ext4 (default ext2) oder mkfs.ext4

Welche Filesysteme gibt es noch?

ext2, ext3, ext4
exfat (Nachfolger von fat32)
btrfs
xfs