# Lösungen zu den geführten Übungen

Welcher erweiterte reguläre Ausdruck würde auf eine beliebige E-Mail-Adresse wie info@example.org passen?


egrep "\S+@\S+\.\S+"
\S+ = whitespace char

Welcher erweiterte reguläre Ausdruck würde auschließlich auf eine beliebige IPv4-Adresse in Punktnotation passen, wie 192.168.15.1?


egrep "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"

Wie kann der Befehl grep verwendet werden, um den Inhalt der Datei /etc/services aufzulisten, wobei alle Kommentare (Zeilen, die mit # beginnen) verworfen werden?


grep -v ^# /etc/services

Die Datei domains.txt enthält eine Liste von Domainnamen, einen pro Zeile. Wie würde der Befehl egrep benutzt, um nur .org- oder .com-Domains aufzulisten?


egrep ".org$|.com$" domains.txt

# Lösungen zu den geführten Übungen

Der Befehl uptime -s zeigt das letzte Datum an, an dem das System eingeschaltet wurde, wie in 2019-08-05 20:13:22. Was wird das Ergebnis des Befehls uptime -s | sed -e 's/(.*) (.*)/\1/' sein?


Es wird ein Fehler auftreten. Standardmäßig sollten Klammern escaped werden, um Rückverweise in sed zu verwenden.

Welche Option sollte man grep geben, damit es übereinstimmende Zeilen zählt, anstatt diese anzuzeigen?


Option -c.

# Eigene Fragen

Wodurch unterscheidet sich reguläres Regex von erweitertem Regex?


In erweitertem Regex haben Symbole wie + standardmäßig ihre Quantifier Bedeutung. In regulärem Regex, müssen Sie durch \ escaped werden.

Mit welchen Befehl führ man in grep Extended Regex aus?


grep -E

Erkläre die Bedutung von den folgenden Regex Ausdrücken: ?,*,+


-   ?         
    0 or 1 occurences
    ab?c = ac abc
-   *         
    0 or more occurences    
    ab*c = ac abc abbbbbbbbbbbbbc
-   +         
    1 or more occurences    
    ab+c = abc abbbbbbc abbbbbbbbbbbbc

Erkläre die Bedutung von den folgenden Regex Ausdrücken: {n}, {min,}, {min,max}


-   {n}       
    exactly n occurences    
    ab{4}c = abbbbc
-   {min,}    
    minimum occurences      
    ab{3,} = abbbc abbbbbbbbbbbc
-   {min,max} 
    min-max occurences      
    ab{3,6} = abbbc abbbbbbc

Erkläre die Bedutung von den folgenden Regex Ausdrücken: $, ^, .


-   $     
    end of line
-   ^     
    start of line
-   .     
    single char

Erkläre die Bedutung von den folgenden Regex Ausdrücken: |, ()


-   |     
    or          
    (grey|gray)
-   ()    
    grouping    
    gr(e|a)y