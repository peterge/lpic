# Lösungen zu den geführten Übungen

Wie lautet der Befehl, um ein Paket namens package.deb mit dpkg zu installieren?

<br>

Übergeben Sie den Parameter -i an dpkg:

    dpkg -i package.deb

Finden Sie mittels dpkg-query heraus, welches Paket eine Datei namens 7zr.1.gz enthält.

<br>

Fügen Sie den Parameter -S zu dpkg-query hinzu (für search):

    dpkg-query -S 7zr.1.gz

Können Sie ein Paket namens unzip mit dpkg -r unzip aus dem System entfernen, wenn das Paket file-roller davon abhängt? Wenn nicht, was wäre dann der richtige Weg, dies zu tun?

<br>

Nein. dpkg löst keine Abhängigkeiten auf und lässt Sie ein Paket nicht entfernen, wenn ein anderes installiertes Paket davon abhängt. In diesem Beispiel könnten Sie zuerst file-roller entfernen (vorausgesetzt, nichts hängt davon ab) und dann unzip entfernen, oder Sie könnten beide gleichzeitig entfernen:

    dpkg -r unzip file-roller

Wie können Sie mit dem Dienstprogramm apt-file herausfinden, welches Paket die Datei /usr/bin/unrar enthält?

<br>

Verwenden Sie den Parameter search gefolgt vom Pfad (oder Dateinamen):

    apt-file search /usr/bin/unrar

Wie lautet der Befehl, um mit apt-cache Informationen für das Paket gimp anzuzeigen?

<br>

Verwenden Sie den Parameter show gefolgt vom Paketnamen:

    apt-cache show gimp

# Lösungen zu den offenen Übungen

Ein Repository mit Debianquellpaketen für die xenial-Distribution, gehostet auf http://us.archive.ubuntu.com/ubuntu/ und mit Paketen für die universe-Komponente soll auf einem System zur Verfügung gestellt werden. Wie lautet der entsprechende Eintrag, welcher in der Datei /etc/apt/sources.list vorgenommen werden muss?

<br>

Quellpakete sind vom Typ deb-src, daher muss die Zeile wie folge lauten:

    deb-src http://us.archive.ubuntu.com/ubuntu/ xenial universe

Diese Zeile könnte auch innerhalb einer .list-Datei in /etc/apt/sources.list.d/ hinzugefügt werden. Der Name ist frei wählbar, sollte aber aussagekräftig sein, wie etwas xenial_sources.list.

Beim Kompilieren eines Programms stoßen Sie auf eine Fehlermeldung, dass die Headerdatei zzip-io.h nicht auf Ihrem System vorhanden ist. Wie können Sie herausfinden, welches Paket diese Datei zur Verfügung stellt?

<br>

Verwenden Sie apt-file search, um herauszufinden, welches Paket eine Datei enthält, die nicht im System vorhanden ist:

    apt-file search zzip-io.h

Wie können Sie eine Abhängigkeitswarnung ignorieren und ein Paket mit dpkg entfernen, selbst wenn es im System Pakete gibt, die davon abhängen?

<br>

Der Parameter --force kann verwendet werden, aber dies sollte nie erfolgen, es sei denn, Sie wissen genau, was Sie tun. Denn es besteht große Gefahr, dass Ihr System in einem inkonsistenten oder “defekten” Zustand hinterlassen wird.

Wie können Sie mit apt-cache mehr Informationen über ein Paket namens midori erhalten?

<br>

Verwenden Sie apt-cache show, gefolgt von dem Paketnamen:

    apt-cache show midori

Welcher Befehl sollte vor der Installation oder Aktualisierung von Paketen mit apt-get benutzt werden, um sicherzustellen, dass der Paketindex aktuell ist?

<br>

apt-get update sollte verwendet werden. Dadurch werden die neuesten Paketindizes von den Repositories heruntergeladen, die in den Repolisten vorhanden sind.

# Eigene Fragen

Nenne zwei Möglichkeiten, wo ein Repository für APT hinterlegt werden kann!

<br>

    /etc/apt/sources.list oder in /etc/apt/sources.list.d/*.list

Wo liegt der cache von apt?

<br>

    /var/cache/apt/...

Wie entfernt man diesen?

<br>

    apt clean

[Nächste Lektion Fragen](questions102.5.md)