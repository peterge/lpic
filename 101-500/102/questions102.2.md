# Lösungen zu den geführten Übungen

Was ist der Standardspeicherort für die GRUB 2 Konfigurationsdatei?

<br>

/boot/grub/grub.cfg

Welche Schritte sind erforderlich, um die Einstellungen für GRUB 2 zu ändern?

<br>

Nehmen Sie Ihre Änderungen in der Datei /etc/default/grub vor, dann aktualisieren Sie die Konfiguration mit update-grub.

In welche Datei sollen benutzerdefinierte GRUB 2 Menüeinträge eingefügt werden?

<br>

/etc/grub.d/40_custom

Wo sind die Menüeinträge für GRUB Legacy gespeichert?

<br>

/boot/grub/menu.lst

Wie können Sie von einem GRUB 2 oder GRUB Legacy Menü aus in die GRUB Shell gelangen?

<br>

Durch drücken von c im Menübildschirm.

# Lösungen zu den offenen Übungen

Stellen Sie sich einen Benutzer vor, der GRUB Legacy so konfiguriert, dass das System von der zweiten Partition der ersten Platte bootet. Er legt dazu den folgenden benutzerdefinierten Menüeintrag an:

```
title My Linux Distro
root (hd0,2)
kernel /vmlinuz root=/dev/hda1
initrd /initrd.img
```

Das System bootet jedoch nicht. Wo könnte der Fehler liegen?

<br>

Die Bootpartition ist falsch. Denken Sie daran, dass GRUB Legacy im Gegensatz zu GRUB 2 die Partitionen von Null an zählt. Der korrekte Befehl für die zweite Partition der ersten Platte muss also root (hd0,1) lauten.

Stellen Sie sich vor, Sie haben eine als /dev/sda identifizierte Festplatte mit mehreren Partitionen. Welcher Befehl kann verwendet werden, um herauszufinden, welche die Bootpartition des Systems darstellt?

<br>

Verwenden Sie fdisk -l /dev/sda. Die Bootpartition wird in der Auflistung mit einem Sternchen (*) gekennzeichnet.

Welcher Befehl kann verwendet werden, um die UUID einer Partition anzuzeigen?

<br>

Verwenden Sie ls -la /dev/disk/by-uuid/ und suchen Sie nach der UUID, die auf die Partition zeigt.

Betrachten Sie den folgenden Eintrag für GRUB 2

```
menuentry "Default OS" {
    set root=(hd0,1)
    linux /vmlinuz root=/dev/sda1 ro quiet splash
    initrd /initrd.img
}
```
Ändern Sie den Eintrag so, dass das System von einer Festplatte mit der UUID 5dda0af3-c995-481a-a6f3-46dcd3b6998d bootet.

<br>

Die Anweisung set root muss geändert werden. Anstatt eine Festplatte und eine Partition anzugeben, weisen Sie Grub an, nach der Partition mit der gewünschten UUID zu suchen.

```
menuentry "Default OS" {
    search --set=root --fs-uuid 5dda0af3-c995-481a-a6f3-46dcd3b6998d
    linux /vmlinuz root=/dev/sda1 ro quiet splash
    initrd /initrd.img
}
```

Wie können Sie GRUB 2 so einstellen, dass 10 Sekunden gewartet wird, bevor der Standardmenüeintrag gestartet wird?

<br>

Fügen Sie den Parameter GRUB_TIMEOUT=10 zu /etc/default/grub hinzu.

Wie lauten die Befehle, um GRUB von einer GRUB Legacy Shell aus auf die erste Partition der zweiten Platte zu installieren?

<br>

```
grub> root (hd1,0)
grub> setup (hd1)
```

# Eigene Fragen

Was befindet sich alles in /boot?

<br>

```
Konfigurationsdatei         config-5.15.14-200.fc35.x86_64
Systemzuordnung             System.map-5.15.14-200.fc35.x86_64
Linux-Kernel                vmlinuz-5.15.14-200.fc35.x86_64
Initiales RAM-Dateisystem   initramfs-5.15.14-200.fc35.x86_64.img
Bootloaderbezogene Dateien  /boot/grub
```

Wie bearbeitet man einen Eintrag in Grub Boot Menü?

<br>

Mit e editiert man einen Eintrag.

Wie bootet man einen editierten Boot Eintrag in Grub?

<br>
  
Strg X oder F10

Welchen Command führt update-grub eigentlich aus? (which update-grub | cat)

<br>

grub-mkconfig -o /boot/grub/grub.cfg

Nenne 6 Konfigurationsmöglichkeiten von /etc/default/grub!

(GRUB_DEFAULT, GRUB_SAVEDEFAULT, GRUB_TIMEOUT, GRUB_CMDLINE_LINUX, GRUB_CMDLINE_LINUX_DEFAULT, GRUB_ENABLE_CRYPTODISK)

<br>

```
GRUB_DEFAULT=
  Der Standardmenüeintrag zum Booten. Numerische Werte oder saved
GRUB_SAVEDEFAULT=
  Wenn diese Option auf true und GRUB_DEFAULT= auf saved gesetzt ist, dann wird die Standard-Bootoption immer die zuletzt im Bootmenü gewählte sein.
GRUB_TIMEOUT=
  Die Zeitüberschreitung in Sekunden, bevor der Standardmenüeintrag ausgewählt wird. 0 = default, -1 = infinite wait
GRUB_CMDLINE_LINUX=
  Hier werden Befehlszeilenoptionen aufgelistet, die den Einträgen für den Linux-Kernel hinzugefügt werden.
GRUB_CMDLINE_LINUX_DEFAULT=
  Fügt nur dem Standardeintrag Optionen hinzu.
GRUB_ENABLE_CRYPTODISK=
  Wenn sie auf y gesetzt sind, suchen Befehle wie grub-mkconfig, updategrub und grub-install nach verschlüsselten Festplatten und fügen die Befehle hinzu, die für den Zugriff darauf während des Bootens benötigt werden. Dies deaktiviert das automatische Booten, da eine Passphrase benötigt wird, um die Platten zu entschlüsseln, bevor auf sie zugegriffen werden kann.
```

[Nächste Lektion Fragen](questions102.3.md)