# Lösungen zu den geführten Übungen

Wenn Sie rpm auf einem Red Hat Enterprise Linux System verwenden, wie wird das Paket file-roller-3.28.1-2.el7.x86_64.rpm installiert, samt Anzeige eines Fortschrittsbalkens während der Installation?

<br>

Verwenden Sie den Parameter -i, um ein Paket zu installieren, und die Option -h, um “Hashmarkierungen” zu aktivieren, die den Fortschritt der Installation anzeigen. Die Antwort lautet also: rpm -ih file-roller-3.28.1-2.el7.x86_64.rpm.

Finden Sie mit rpm heraus, welches Paket die Datei /etc/redhat-release enthält.

<br>

Sie fragen Informationen über eine Datei ab, also benutzen Sie den -qf-Parameter: rpm -qf /etc/redhat-release.

Wie würden Sie yum benutzen, um nach Aktualisierungen für alle Pakete im System zu suchen?

<br>

Verwenden Sie die Operation check-update ohne einen Paketnamen: yum check-update.

Wie würden Sie mit zypper ein Repository namens repo-extras deaktivieren?

<br>

Benutzen Sie den Befehl modifyrepo und die Option -d, um das Repo zu deaktivieren: zypper modifyrepo -d repo-extras.

Wenn Sie eine .repo-Datei haben, die ein neues Repository beschreibt, wo sollte diese Datei abgelegt werden, damit sie vom DNF erkannt wird?

<br>

.repo-Dateien für DNF sollten an der gleichen Stelle abgelegt werden, wie jene die von YUM benutzt werden und zwar innerhalb von /etc/yum.repos.d/.

# Lösungen zu den offenen Übungen

Wie würden Sie zypper benutzen, um herauszufinden, welchem Paket die Datei /usr/sbin/swapon gehört?

<br>

Verwenden Sie den Operator se (search) und die Option --provides: zypper se --provides /usr/sbin/swapon.

Wie können Sie mit dnf eine Liste aller im System installierten Pakete erhalten?

<br>

Verwenden Sie den Operator list, gefolgt von der Option --installed: dnf list --installed.

Wie lautet der Befehl, mit dem dnf ein Repository unter https://www.example.url/home:reponame.repo dem System hinzufügt?

<br>

Die Arbeit mit Repositories ist eine “Konfigurationsänderung”, also benutzen Sie den config-manager und die --add_repo Option: dnf config-manager --add_repo https://www.example.url/home:reponame.repo.

Wie können Sie zypper benutzen, um zu überprüfen, ob das Paket unzip installiert ist?

<br>

Sie müssen eine Suche (se) betreffend der installierten (-i) Paketen durchführen: zypper se -i unzip.

<br>

Finden Sie mit yum heraus, welches Paket die Datei /bin/wget zur Verfügung stellt.

<br>

Um herauszufinden, was eine Datei bereitstellt, verwenden Sie whatprovides und den Dateinamen: yum whatprovides /bin/wget.


# Eigene Fragen

Welche Config Datei nutzt yum?

<br>

/etc/yum.conf

Wo legt man für yum ein Repo an?

<br>

/etc/yum.repo.d/

Welche Endungen haben diese Listen?

<br>

.repo

Wie updatet man ein Paket mit rpm?

<br>

rpm -U XXX.rpm

Wo ist der unterschied zwischen yum update und apt update?

<br>

Yum update updatet Pakete auch, es ist quasi apt update und apt upgrade vereint.


Wie kann man mit yum herrausfinden, welches Paket die Datei libgimpui-2.0.so.0 zur Verfügung stellt?

<br>

yum whatprovides libgimpui-2.0.so.0

Wie installiert man ein XXX.rpm Paket mit rpm?

<br>

Mit rpm -i XXX.rpm

Wie kann man mit rpm verifizieren, zu welchem Paket eine Datei gehört?

<br>

rpm -qf /etc/protocols

Wie kann man mit rpm verifizieren, ob alle Dateien eines Paketes an der korrekten Stelle existieren?

<br>

rpm --verify XXX

Wie entfernt man Pakete mit yum und rpm?

<br>

yum remove wget, rpm -e wget (für erase)

Wie lautet die Syntax zum installieren, suchen und entfernen von Paketen bei zypper?

<br>

zypper in, zypper se und zypper rm

[Nächste Lektion Fragen](questions102.6.md)