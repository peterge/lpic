# Lösungen zu den geführten Übungen

Wie könnte der Befehl telinit verwendet werden, um das System neu zu starten?

<br>

Der Befehl telinit 6 wechselt zu Runlevel 6, d.h. das System wird neu gestartet.

Was passiert mit den Diensten, die sich auf die Datei /etc/rc1.d/K90network beziehen, wenn das System Runlevel 1 aktiviert?

<br>

Aufgrund des Buchstabens K am Anfang des Dateinamens werden die entsprechenden Dienste beendet.

Wie könnte ein Benutzer mit dem Befehl systemctl überprüfen, ob die Unit sshd.service läuft?

<br>

Mit dem Befehl systemctl status sshd.service oder systemctl is-active sshd.service.

Basierend auf der Nutzung von systemd: Welcher Befehl muss ausgeführt werden, um die Aktivierung von sshd.service während der Systeminitialisierung zu ermöglichen?

<br>

Der Befehl systemctl enable sshd.service wird von root ausgeführt.

# Lösungen zu den offenen Übungen

In einem SysV-basierten System wird angenommen, dass der in /etc/inittab definierte Standard-Runlevel 3 ist, das System aber immer im Runlevel 1 startet. Was ist die wahrscheinliche Ursache dafür?

<br>

Die Parameter 1 oder S können in der Parameterliste des Kernels vorhanden sein.

Obwohl die Datei /sbin/init in systemd-basierten Systemen gefunden werden kann, ist sie nur ein symbolischer Link zu einer anderen ausführbaren Datei. Worauf zeigt in solchen Systemen die Datei mit /sbin/init?

<br>

Die Hauptsystembinärdatei: /lib/systemd/systemd.

Wie kann das Standardsystemziel in einem systemd-basierten System verifiziert werden? 2 Möglichkeiten!

<br>

Der symbolische Link /etc/systemd/system/default.target verweist auf die Unit-Datei, die als Standardziel definiert ist. Der Befehl systemctl get-default kann ebenfalls verwendet werden.

Wie kann ein mit dem Befehl shutdown eingeplanter Systemneustart abgebrochen werden?

<br>

Der Befehl shutdown -c sollte verwendet werden.

# Lösungen zu den eigenen Fragen

Welche PID hat der systembasierte Servicemanager normalerweise?

<br>

Der Servicemanager ist das erste Programm, welches vom Kernel während des Bootprozesses gestartet wird, daher ist seine PID (Prozess-Identifikationsnummer) immer 1.

Nenne die verschiedeten Runlevels!

<br>

    SysVInit (entspricht quasi den Runlevels unter systemdb)
    Runlevel 0                  Systemabschaltung.
    Runlevel 1, s oder single   Einzelbenutzermodus, ohne Netzwerk und andere nicht wesentliche Funktionen (Wartungsmodus).
    Runlevel 2, 3 oder 4        Mehrbenutzer-Modus. Benutzer können sich per Konsole oder Netzwerk anmelden. Die Runlevel 2 und 4 werden nicht häufig verwendet.
    Runlevel 5                  Mehrbenutzer-Modus. Entspricht 3, inklusive der Anmeldung im grafischen Modus.
    Runlevel 6                  Systemneustart.

In welchen Ordnern liegen die Skripte die bei jedem Runlevel, und bei den einzelnen Runlevels ausgeführt werden?

<br>

/etc/rc1-6.d sind symbolische Links zu den eigentlichen Skripten in /etc/init.d.

Mit welchem Befehl kann man das aktuelle Runlevel anzeigen?

<br>

runlevel
N 5
Das N zeigt an, dass sich der Runlevel seit dem letzten Bootvorgang nicht geändert hat.

Nenne die Befehle zum Starten, Stoppen, Neustarten und zum Anzeigen von Informationen über einen Service!

<br>

systemctl start unit.service, systemctl stop unit.service, systemctl restart unit.service und systemctl status unit.service

Nenne den Befehl zum Anzeigen, ob ein Service beim Boot automatisch startet!

<br>

systemctl is-enabled unit.service

Wie wechselt man ohne systemctl zwischen einem Runlevel?

<br>

init oder telinit

Wie wechselt man mit systemctl zwischen einem Runlevel?

<br>

systemctl isolate multi-user.target

Wie setzt man mit systemctl ein Runlevel als Standard beim nächsten Boot?

<br>

systemctl set-default multi-user.target

Wie sendet man eine Nachricht an alle angemeldeten User?

<br>

wall Rebooting!

Wie startet man unter Verwendung des Befehls shutdown den Computer neu?

<br>

shutdown -r

Wie führt man das Herunterfahren des PCs sofort aus und was ist der normalerweise verwendete Standard?

<br>

shutdown now, 1 Minute

[Nächste Lektion Fragen](101-500/102/questions102.1.md)